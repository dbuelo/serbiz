-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2021 at 03:54 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `serbiz`
--
CREATE DATABASE IF NOT EXISTS `serbiz` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `serbiz`;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` text NOT NULL,
  `archive` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_name`, `archive`) VALUES
(1, 'Honda', 0),
(2, 'Fuso', 0),
(3, 'Isuzu', 0),
(4, 'Toyota', 0),
(5, 'Hyundai', 0);

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

DROP TABLE IF EXISTS `chats`;
CREATE TABLE IF NOT EXISTS `chats` (
  `chat_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `time_sent` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`chat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`chat_id`, `sender_id`, `receiver_id`, `message`, `time_sent`, `status`) VALUES
(1, 2, 2, 'Title : Query for \"Test Item 2\"\n\ntertert', '2020-02-20 22:41:57', 1),
(2, 2, 0, 'Title : Query for \"Test Item 1\"\n\ntest', '2020-02-21 19:16:21', 0),
(3, 2, 2, 'Title : Query for \"Test Item 2\"\n\ntwtetsdfasdfasdfasdf', '2020-02-21 19:19:20', 1),
(4, 0, 3, 'test', '2020-02-24 18:31:04', 0),
(5, 0, 2, 'test', '2020-02-24 18:33:10', 1),
(6, 3, 2, 'test23423423', '2020-02-24 18:35:25', 0),
(7, 3, 3, 'eteststese', '2020-02-24 18:35:37', 0),
(8, 2, 3, '', '2020-02-25 20:12:58', 0),
(9, 2, 2, 'testest', '2020-02-25 20:13:09', 1),
(10, 2, 2, 'test', '2020-02-25 20:13:17', 1),
(11, 2, 3, 'test', '2020-02-25 20:13:25', 0),
(12, 7, 4, 'test', '2020-03-01 18:11:28', 1),
(13, 2, 0, 'Notification:<br><br>Your reservation for the item Test Item 1 on 2020-02-29', '2020-03-07 17:19:42', 0),
(14, 2, 0, 'Notification:<br><br>Your reservation for the item Test Item 1 on 2020-02-20', '2020-03-07 17:20:16', 0),
(15, 2, 0, 'Notification:<br><br>Your reservation for the item Test Item 1 on 2020-02-20', '2020-03-07 17:20:40', 0),
(16, 2, 0, 'Notification:<br><br>Your reservation for the item Test Item 1 on 2020-02-29', '2020-03-07 17:21:01', 0),
(17, 2, 2, 'Notification:<br><br>Your reservation for the item Test Item 1 on 2020-02-20', '2020-03-07 17:29:00', 1),
(18, 2, 2, 'Notification:<br><br>Your reservation for the item Test Item 1 on 0000-00-00 was approved.', '2020-03-12 22:38:42', 1),
(19, 2, 2, 'Notification:<br><br>Your reservation for the item Test Item 1 on 2020-02-26 was Declined. Please reserve on another day.', '2020-03-12 22:40:38', 1),
(20, 2, 2, 'Notification:<br><br>Your reservation for the item Test Item 1 on 2020-02-26 was Declined. Please reserve on another day.', '2020-03-12 22:41:18', 1),
(21, 2, 2, 'Notification:<br><br>Your reservation for the item Test Item 1 on 2020-02-29 was Declined. Please reserve on another day.', '2020-03-12 22:41:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `item_price` text NOT NULL,
  `brand` int(11) NOT NULL,
  `model` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `images` text NOT NULL,
  `owner_id` int(11) NOT NULL,
  `capacity` int(11) NOT NULL DEFAULT 0,
  `archive` int(11) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item_name`, `item_description`, `item_price`, `brand`, `model`, `status`, `images`, `owner_id`, `capacity`, `archive`, `date_added`) VALUES
(1, 'Test Item 1', 'Test Description 1', '1235', 0, '', 1, '', 0, 0, 0, '2020-02-15 11:22:55'),
(2, 'Test Item 2', 'Test Description 2', '1000', 2, 'YD-2003', 1, '[\"serbiz_7.jpg\",\"serbiz_8.jpg\"]', 2, 0, 0, '2020-02-15 11:22:55'),
(3, 'Test Item 4', 'Test Description 4', '342', 5, '3', 1, '[\"serbiz_9.jpg\",\"serbiz_10.jpg\"]', 2, 0, 0, '2020-02-15 11:22:55'),
(4, 'Test Item 1', 'atea', '2000', 2, 'YD-2013-0', 1, '[\"serbiz_11.jpg\",\"serbiz_12.jpg\"]', 2, 0, 0, '2020-02-15 11:22:55'),
(5, 'Test Item 1', 'test', '234', 2, '32', 0, '[]', 2, 0, 0, '2020-02-15 11:22:55'),
(6, 'Test Item 1', 'test', '234', 2, '32', 0, '[]', 2, 0, 0, '2020-02-15 11:22:55'),
(7, 'werw', 'awdf', '234', 1, '', 0, '[]', 2, 0, 0, '2020-02-15 11:22:55'),
(8, 'ewte', 'sdfa', '234', 2, '123123', 0, '[]', 2, 0, 0, '2020-02-15 11:22:55'),
(9, '', '', '0', 0, '', 0, '[]', 2, 0, 0, '2020-02-15 11:22:55'),
(10, 'Honda Civic', 'Item Capacity is 20 person per can', '1000', 1, 'YD-2013', 0, 'Array', 5, 0, 0, '2020-03-15 13:24:36'),
(11, 'Honda Civic', 'Item Capacity is 20 person per can', '1000', 1, 'YD-2013', 0, 'Array', 5, 0, 0, '2020-03-15 13:27:10'),
(12, 'Honda Civic', 'Item Capacity is 20 person per can', '1000', 1, 'YD-2013', 0, 'Array', 5, 0, 0, '2020-03-15 13:28:17'),
(13, 'Honda Civic', 'Item Capacity is 20 person per can', '1000', 1, 'YD-2013', 0, 'Array', 5, 0, 0, '2020-03-15 13:32:02'),
(14, 'Honda Civic', 'Item Capacity is 20 person per can', '1000', 1, 'YD-2013', 0, 'Array', 5, 0, 0, '2020-03-15 13:32:39'),
(15, 'Honda Civic', 'Item Capacity is 20 person per can', '1000', 1, 'YD-2013', 0, 'Array', 5, 0, 0, '2020-03-15 13:33:22'),
(16, 'Honda Civic', 'Item Capacity is 20 person per can', '1000', 1, 'YD-2013', 0, 'Array', 5, 0, 0, '2020-03-15 13:33:26'),
(17, 'Honda Civic', 'Item Capacity is 20 person per can', '1000', 1, 'YD-2013', 0, '\"serbiz_7.png\"', 5, 0, 0, '2020-03-15 13:33:51'),
(18, 'mutibize monterossss', 'This is a test Item', '3000', 5, 'YD-2013', 0, '\"serbiz_14.jpg\"', 5, 0, 0, '2020-03-15 13:40:02'),
(19, 'toyota', 'this is one year old toyota', '10000', 4, 'YD-2014', 0, '\"serbiz_16.jpg\"', 5, 5, 0, '2020-03-15 13:42:52'),
(20, 'toyota', 'this is one year old toyota', '10000', 4, 'YD-2014', 0, '[\"serbiz_17.jpg\",\"serbiz_18.jpg\"]', 5, 5, 0, '2020-03-15 13:43:19'),
(21, 'TRes', 'awerasdfasd', '1231', 2, 'werwe', 0, '[]', 5, 0, 0, '2020-03-15 14:12:58'),
(22, 'test item 1', 'test Item 1', '342', 3, '32', 1, '[\"serbiz_19.jpg\"]', 2, 2, 0, '2020-03-15 16:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
CREATE TABLE IF NOT EXISTS `reservations` (
  `reservation_id` int(11) NOT NULL AUTO_INCREMENT,
  `reserve_to` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `date_reserved` date NOT NULL,
  `date_approved` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `payment` int(11) NOT NULL,
  `archive` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`reservation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`reservation_id`, `reserve_to`, `item_id`, `date_reserved`, `date_approved`, `status`, `payment`, `archive`) VALUES
(7, 4, 2, '2020-03-17', '0000-00-00', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `comment` longtext NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`review_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`review_id`, `item_id`, `user_id`, `rate`, `name`, `comment`, `date`) VALUES
(1, 1, 2, 0, '', 'test', '2020-02-20 21:03:56'),
(2, 2, 2, 0, '', 'test', '2020-02-20 21:08:17'),
(3, 2, 2, 0, '', '', '2020-02-20 21:30:17'),
(4, 2, 2, 0, '', 'teste', '2020-02-20 22:34:24');

-- --------------------------------------------------------

--
-- Table structure for table `run_increments`
--

DROP TABLE IF EXISTS `run_increments`;
CREATE TABLE IF NOT EXISTS `run_increments` (
  `run_increments_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`run_increments_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `run_increments`
--

INSERT INTO `run_increments` (`run_increments_id`, `file_name`, `date`) VALUES
(1, '../database/alter_table_items_insert_date_added_column.sql', '2020-02-15 11:22:55'),
(2, '../database/create_items_table.sql', '2020-02-15 11:22:56'),
(3, '../database/01-update-tbl_users.sql', '2021-10-12 19:43:55'),
(4, '../database/02-alter-item-price-table.sql', '2021-10-12 19:43:55');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(150) NOT NULL,
  `user_pass` varchar(150) NOT NULL,
  `user_fname` varchar(150) NOT NULL,
  `user_contact` int(140) NOT NULL,
  `user_email` varchar(140) NOT NULL,
  `confirm` int(11) NOT NULL,
  `address` text NOT NULL,
  `contact_no` text NOT NULL,
  `role` int(11) NOT NULL,
  `remove` int(11) NOT NULL DEFAULT 0,
  `attach` text NOT NULL,
  `profile_img` text NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `user_name`, `user_pass`, `user_fname`, `user_contact`, `user_email`, `confirm`, `address`, `contact_no`, `role`, `remove`, `attach`, `profile_img`) VALUES
(1, '', '3750c667d5cd8aecc0a9213b362066e9', 'darwin', 0, 'dbuelo@gmail.com', 1, '', '', 5, 0, '', ''),
(2, '', '3750c667d5cd8aecc0a9213b362066e9', 'Darwin', 0, 'test@gmail.com', 1, '', '', 1, 0, '', ''),
(4, '', '3750c667d5cd8aecc0a9213b362066e9', 'test2@gmail.com', 0, 'test2@gmail.com', 1, '', '', 0, 0, '', ''),
(5, '', '12345', 'test4@gmail.com', 0, 'test4@gmail.com', 1, '', '', 1, 0, '', ''),
(6, '', '12345', 'test6@gmail.com', 0, 'test6@gmail.com', 0, '', '', 0, 0, '', ''),
(7, '', '12345', 'test7@gmail.com', 0, 'test7@gmail.com', 0, '', '', 0, 0, '', '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
