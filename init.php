<?php
// ! IMPORTANT Uncomment this on the release to disable all alerts
//error_reporting(0);
require_once 'autoloader.php';
$companyname = '';
$companydesc = '';
$Outline = new Layout($companyname, $companydesc);
session_start();
$css = [
    'vendor/fontawesome-free-5.12.1-web/css/all.css',
    'css/font-awesome.min.css',
    'css/bootstrap-337.min.css',
    'css/style1.css',
    'css/main.css',
    'css/components.css',
    'css/icons.css',
    'css/responsee.css',
    'css/template-style.css',
    'css/custom.css',
    'css/responsive.css',
    'https://fonts.googleapis.com/icon?family=Material+Icons',
    'https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap',
    'css/modals.css'
];

$js = [
    'js/jquery-1.8.3.min.js',
    'vendor/jquery/jquery-3.2.1.min.js',
    'js/jquery-ui.min.js',
    'js/responsee.js',
    'vendor/bootstrap/js/popper.min.js',
    'vendor/bootstrap/js/tooltip.js',
    'vendor/bootstrap/js/bootstrap.min.js',
];

$Outline->addCSS($css);
$Outline->addJS($js,false);
$Outline->addJS(['js/modalHandler.js','js/general.js']);
// set the current user
if (isset($_SESSION['User'])) {
    $User = unserialize($_SESSION['User']);
} else {
    $User = false;
}

