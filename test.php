<?php
require_once 'init.php';
$Outline->header('test');
?>
<div class="border-dark">
    <input type="text" id="clientId" style="border:1px solid black;">
</div>
<div class="border-dark">
    <input type="text" id="mainInput" class="form-control-file" list="browsers" style="border:1px solid black;">
</div>
<datalist id="browsers">
    <option id="Internet Explore" data-id="1" value="Internet Explorer">
    <option id="Firefox" data-id="2" value="Firefox">
    <option id="Chrome" data-id="3" value="Chrome">
    <option id="Opera" data-id="4" value="Opera">
    <option id="Safari" data-id="5" value="Safari">
</datalist>

<script>
    $(document).ready(function () {
        $('#forClientInput').blur(function () {
            let inputValue = $(this).val();
            let list = $('#forClientInput');
            var option = list.find("[value='" + inputValue + "']");

            if (option.length > 0) {
                var id = option.data("id");
                $('#client_id').val(id);
            }
        });
    });
</script>

<?php
$Outline->footer();
?>