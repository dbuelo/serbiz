<?php

class Layout
{
    const LOAD_IN_FOOTER = 1;
    const LOAD_IN_HEADER = 2;

    protected $css = [
    ];
    protected $js = [
    ];
    protected $companyName = 'Test Company';
    protected $companyDesc = 'Test Description';
    protected $companyLogo = null;

    function __construct($companyName = null, $companyDesc = null, $css = null, $js = null)
    {
        $this->setCompanyName($companyName);
        if (!empty($companyDesc)) {
            $this->$companyDesc = $companyDesc;
        }
        if (!empty($css)) {
            $this->addCSS($css);
        }
        if (!empty($js)) {
            $this->addJS($js);
        }
    }

    /**
     * Create the header of the system
     */
    public function header($page = null, $includeHeader = true)
    {
        $html = '
        <!DOCTYPE HTML>
        <html lang="en-US">
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Unicat project">
        <meta name="viewport" content="width=device-width, initial-scale=1"> ' . PHP_EOL;
        if (!empty($this->companyLogo)) {
            $html .= '<link rel="icon" type="image/png" href="' . $this->companyLogo . '">' . PHP_EOL;
        }
        foreach ($this->css as $css) {
            $html .= '<link rel="stylesheet" type="text/css" href="' . $css . '">' . PHP_EOL;
        }
        $html .= $this->prepareJS(static::LOAD_IN_HEADER);
        $pageTitle = isset($page) ? $page : $this->companyName;
        $html .= "<title>{$pageTitle}</title>";
        $html .= '</head><body>';
        echo $html;
        if ($includeHeader === true) {
            $this->navigationBar();
        }
    }

    /**
     * Create the footer and include the JS needed in the system
     */
    public function footer()
    {
        $html = $this->prepareJS(static::LOAD_IN_FOOTER);
        $html .= '<div class="row spacer"><div class="col-md-12 mt-5">&nbsp</div></div></body></html>';
        echo $html;
    }

    /**
     * @param $path string / array
     */
    public function addCSS($path)
    {
        if (is_array($path)) {
            foreach ($path as $file) {
                $this->css[] = $file;
            }
        } else {
            $this->css[] = $path;
        }
    }

    /**
     * @param $path string /array
     */
    public function addJS($path, $footer = true)
    {
        $destination = $footer ? 'footer' : 'header';
        if (is_array($path)) {
            foreach ($path as $file) {
                $this->js[$destination][] = $file;
            }
        } else {
            $this->js[$destination][] = $path;
        }
    }

    private function prepareJS($destination)
    {
        $html = '';
        $js = $this->js;
        if ($destination == static::LOAD_IN_FOOTER) {
            $location = 'footer';
        } else {
            $location = 'header';
        }
        if (isset($js[$location]) && count($js[$location]) > 0) {
            foreach ($js[$location] as $file) {
                $html .= "<script src='{$file}'></script>" . PHP_EOL;
            }
        }
        return $html;
    }

    public function loadJS($destination)
    {
        $html = $this->prepareJS($destination);
        print $html;
    }

    /**
     * @param $name string
     */
    public function setCompanyName($name)
    {
        $this->companyName = $name;
    }

    /**
     * @param $desc string
     */
    public function setCompanyDescription($desc)
    {
        $this->companyDesc = $desc;
    }

    /**
     * @return array
     */
    public function getCSS()
    {
        return $this->css;
    }

    /**
     * @return array
     */
    public function getJS()
    {
        return $this->js;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function navigationBar()
    {
        $pageSegment = [
            'promo' => [ 
                'title' => 'Promo',
                'href' => 'promo.php'
            ],
            'about' => [ 
                'title' => 'About',
                'href' => 'about.php'
            ]
        ];
        $pagesMenu = '';
        foreach ($pageSegment as $page) {
            $pagesMenu.= "<li><a class='align-middle' href='" . $page['href'] . "'>" . $page['title'] . "</a></li>";
        }

        $html = 
        '<header>
            <div id="top">
                <div class="row ">
                    <div class="col-md-3 offer p-5">
                        <img src="upload/defaults/ark_logo_white.png" class="logo">
                    </div>
                    <div class="col-md-7 ">
                        <ul class="main-menu">
                        ' . $pagesMenu . '
                        </ul>
                    </div>
                    <div class="col-md-2 ">
                        <ul class="menu ">
                            <li>
                            <a href="checkout.php">';
                            if (isset($_SESSION['User'])) {
                                $html .= "<a href='logout.php'>Sign-out</a>";
                            } else {
                                $html .= "<a href='register.php'>Register</a> |<a href='login.php'> Login</a>";
                            }
                            $html .= '</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>



            <div id="navbar" class="navbar navbar-default"><!-- navbar navbar-default Begin -->
            <div class="container" id="app"><!-- container Begin -->
            <div class="navbar-header"><!-- navbar-header Begin -->
            <a href="index.php" class="navbar-brand home"><!-- navbar-brand home Begin -->
            <h3 class="hidden-xs">SerBiz</h3>
            <h3 class="visible-xs">SerBiz</h3>
            </a><!-- navbar-brand home Finish -->
            <button class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
            <span class="sr-only">Toggle Navigation</span>
            <i class="fa fa-align-justify"></i>
            </button>
            <button class="navbar-toggle" data-toggle="collapse" data-target="#search">
            <span class="sr-only">Toggle Search</span>
            <i class="fa fa-search"></i>
            </button>
            </div><!-- navbar-header Finish -->
            <div class="navbar-collapse collapse" id="navigation"><!-- navbar-collapse collapse Begin -->
            <div class="padding-nav"><!-- padding-nav Begin -->
            <ul class="nav navbar-nav left"><!-- nav navbar-nav left Begin -->
            <li class="active">
            <a href="index.php">Home</a>
            </li>';

        if (isset($_SESSION['User'])) {
            $User = unserialize($_SESSION['User']);
            $html .= '<li class="active"><a href="messages.php">Message';
            $unreadCount = Chat::CountUnread();
            if ($unreadCount !== 0) {
                $html .= '<span class="badge badge-primary message-badge">' . $unreadCount . '</span>';
            }
            $html .= '</a></li>';
            $html .= ' <li class="active"><a href="profile.php">Profile</a></li>';
            if ($User->getRole() == User::OWNER) {
                $html .= ' <li class="active"><a href="myAccount.php">My Reservations';
                $unreadCount = Reservation::CountReservation();
                if ($unreadCount !== 0) {
                    $html .= '<span class="badge badge-primary message-badge">' . $unreadCount . '</span>';
                }
                $html .= '</a></li>';
                $html .= ' <li class="active"><a href="manageProp.php">My Properties</a></li>';
            }
        }
        $html .= '</ul><!-- nav navbar-nav left Finish --></div><!-- padding-nav Finish -->';

        if (isset($_SESSION['User'])) {
            $User = unserialize($_SESSION['User']);
            if ($User->getRole() == User::OWNER) {
                $html .= '<a href="addItemForRent.php" class="btn navbar-btn btn-primary right">';
            }
        } else {
            $html .= '<a href="login" class="btn navbar-btn btn-primary right">';
        }
        $html .= '<span style="color: #ffffff">Post my Ads</span>';
        $html .= '</a><div class="navbar-collapse collapse right">
            </div><!-- navbar-collapse collapse right Finish -->
            </div><!-- navbar-collapse collapse Finish -->
            </div><!-- container Finish -->
            </div><!-- navbar navbar-default Finish -->
            </header><div class="container">';
        echo $html;
        
        if(isset($_SESSION['message'])){
            $message = $_SESSION['message'];
            unset($_SESSION['message']);
            Util::CreateDialog($message['title'],$message['message']);
        }

    }
}