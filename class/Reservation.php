<?php

class Reservation
{
    const TABLE_NAME = 'reservations';
    const STATUS_PENDING = 4;
    const STATUS_APPROVED = 1;
    const STATUS_DECLINE = 2;
    const STATUS_FINISHED = 3;

    protected $reservationID;
    protected $reserveTo;
    protected $itemID;
    protected $dateReserve;
    protected $dateApproved;
    protected $status;
    protected $payment;
    protected $archive;

    protected $pickUpDate;
    protected $pickOffDate;
    protected $pickUpLocation;
    protected $passengerNo;
    protected $notes;



    public static function LoadArray($ids = [], $status = null, $archive = 0)
    {
        $sql = 'SELECT
                    reservation_id,
                    reserve_to,
                    item_id,
                    date_reserved,
                    date_approved,
                    status,
                    payment,
                    archive,
                    pick_up_date,
                    pick_off_date,
                    pick_up_location,
                    passenger_no,
                    notes
                FROM
                    ' . static::TABLE_NAME . '
                WHERE
                    TRUE
        ';

        if (!empty($ids)) {
            $sql .= 'AND
                        reservation_id IN (' . implode(',', $ids) . ')';
        }

        if (!empty($status)) {
            $sql .= 'AND
                    status =  ' . $status;
        }

        $sql .= 'AND
                    archive = ' . $archive;
        $result = DBcon::execute($sql);
        $datas = DBcon::fetch_all_assoc($result);
        $return = [];
        if (!empty($datas)) {
            foreach ($datas as $data) {
                $new = new static();
                $new->setReservationID($data['reservation_id']);
                $new->setReserveTo($data['reserve_to']);
                $new->setItemID($data['item_id']);
                $new->setDateReserve($data['date_reserved']);
                $new->setDateApproved($data['date_approved']);
                $new->setStatus($data['status']);
                $new->setPayment($data['payment']);
                $new->setArchive($data['archive']);
                $new->setPickUpDate($data['pick_up_date']);
                $new->setPickOffDate($data['pick_off_date']);
                $new->setPickUpLocation($data['pick_up_location']);
                $new->setPassengerNo($data['passenger_no']);
                $new->setNotes($data['notes']);
                $return[$data['reservation_id']] = $new;
            }
            return $return;
        }
        return false;
    }

    public static function IsAvailable($itemID)
    {
        $date = date('Y-m-d');
        $sql = "SELECT 
                     *
               FROM
                    reservations
               WHERE
                    pick_up_date <= '{$date}'
               AND
                    pick_off_date >= '{$date}'
               AND
                    status =" . self::STATUS_APPROVED;
        $result = DBcon::execute($sql);
        $data = DBcon::fetch_assoc($result);
        if(!empty($data)){
            return false; // not available
        }
        return true; // available to the date

    }

    public function getItemDetails()
    {
        return Item::Load($this->itemID);
    }

    public static function ChangeStatus(int $reservationID, int $status, int $itemID, $date)
    {
        $User = User::GetCurrentUser();
        $Reservation = static::Load($reservationID);
        $RenterDetails = $Reservation->getRenterDetails();
        $Item = $Reservation->getItemDetails();
        if ($status == static::STATUS_APPROVED) {
            $selectQuery = 'SELECT
                                reservation_id,
                                reserve_to
                            FROM
                                ' . static::TABLE_NAME . '
                            WHERE
                                status = ' . static::STATUS_APPROVED . '
                            AND
                                item_id = ' . $itemID . '
                            AND
                                date_reserved = ' . $date . '
                            AND
                                archive = 0';
            $Obj = DBcon::execute($selectQuery);
            $result = DBcon::fetch_assoc($Obj);
            if (empty($result)) {
                $updateData = [
                    'status' => $status,
                    'date_approved' => Util::date()
                ];
                DBcon::update(static::TABLE_NAME, $updateData, ['reservation_id' => $reservationID]);
                Util::CreateDialog('Success', 'Item reservation successful');
                //Send a message
                $message = 'Notification:<br><br>Your reservation for the car ' . $Item->getItemName() . ' on ';
                $message .= $Reservation->getDateReserve() . ' was approved with the reservation id of #' . $reservationID;
                $message .= '<br/><br/>';
                $message .= '<b>Renter Name :</b>' . $RenterDetails->getUserFname() . '<br/>';
                $message .= '<b>Contact No :</b>' . $RenterDetails->getContactNo() . '<br/>';
                $message .= '<b>Pick Up Date :</b>' . Util::date($Reservation->getPickUpDate()) . '<br/>';
                $message .= '<b>Pick Off Date :</b>' . Util::date($Reservation->getPickOffDate()) . '<br/>';
                $message .= '<b>Pick Location :</b>' . $Reservation->getPickUpLocation() . '<br/>';
                $message .= '<b>Notes :</b>' . $Reservation->getNotes() . '<br/>';
                Chat::SendMessage(Chat::SENDER_SYSTEM, $Reservation->getReserveTo(), $message);
            } else {
                Util::CreateDialog('Failed', 'Car is already reserve on the date selected');
            }
        } elseif ($status == static::STATUS_DECLINE) {
            $updateData = ['status' => $status,];
            DBcon::update(static::TABLE_NAME, $updateData, ['reservation_id' => $reservationID]);
            $message = '<b>Notification:</b><br><br>Your reservation for the car ' . $Item->getItemName() . ' on ';
            $message .= Util::date($Reservation->getPickUpDate()) . ' was declined. Please reserve on another day.';
            $message .= '<br> Reservation ID: #' . $Reservation->getReservationID();
            Chat::SendMessage(Chat::SENDER_SYSTEM, $Reservation->getReserveTo(), $message);
            Util::CreateDialog('Success', 'The request was Declined');
        } elseif ($status == static::STATUS_PENDING){
            $updateData = ['status' => $status,];
            DBcon::update(static::TABLE_NAME, $updateData, ['reservation_id' => $reservationID]);
            $message = "<b>Notification : </b><br><br>";
            $message .= "Your reservation with reservation id of #" . $reservationID . " , is being review by the car owner.";
            Chat::SendMessage(Chat::SENDER_SYSTEM, $Reservation->getReserveTo(), $message);
            Util::CreateDialog('Success', 'Request has been put into pending status');

        }
    }

    //maybe we can add a return value once the DBcon is executed
    // this will confirm if it was successful on saving
    public function save()
    {
        $data = [
            'reserve_to' => $this->reserveTo,
            'item_id' => $this->itemID,
            'date_reserved' => $this->dateReserve,
            'date_approved' => $this->dateApproved,
            'status' => $this->status,
            'pick_up_date' => $this->getPickUpDate(),
            'pick_off_date' => $this->getPickOffDate(),
            'pick_up_location' => $this->getPickUpLocation(),
            'passenger_no' => $this->getPassengerNo(),
            'notes' => $this->getNotes()
        ];
        if (!isset($this->reservationID)) {
            $return = DBcon::insert(static::TABLE_NAME, $data);
        } else {
            DBcon::update(static::TABLE_NAME, $data, ['reservation_id' => $this->reservationID]);
        }
        $return;
    }

    public static function Load($id)
    {
        $Object = static::LoadArray([$id]);
        return reset($Object);
    }

    public static function Reserve($reservationData)
    {
        $CurrentUser = User::GetCurrentUser();
        $new = new static();
        $new->setPickUpDate($reservationData['pickupDate']);
        $new->setItemID($reservationData['itemID']);
        $new->setReserveTo($CurrentUser->getUserID());
        $new->setPickOffDate($reservationData['pickOffDate']);
        $new->setPickUpLocation($reservationData['pickUpLocation']);
        $new->setPassengerNo($reservationData['passengerNo']);
        $new->setNotes($reservationData['notes']);
        $new->save();
    }

    public static function CountReservation()
    {
        $CurrentUser = User::GetCurrentUser();
        $sql = 'SELECT
                    i.item_id,
                    r.reservation_id
                 FROM
                    reservations r
                 LEFT JOIN
                    items i
                 USING
                    (item_id)
                 WHERE
                    i.owner_id = '. $CurrentUser->getUserID() . '
                AND
                    r.status = 0';
        $object = DBcon::execute($sql);
        $result = DBcon::fetch_all_array($object);
        if (!empty($result)) {
            return count($result);
        }
        return false;
    }

    public function getRenterDetails()
    {
        $RenterDetails = User::Load($this->getReserveTo());
        return $RenterDetails;
    }

    public function setReservationID($reservationID)
    {
        $this->reservationID = $reservationID;
    }

    public function getReservationID()
    {
        return $this->reservationID;
    }

    public function setReserveTo($clientID)
    {
        $this->reserveTo = $clientID;
    }

    public function getReserveTo()
    {
        return $this->reserveTo;
    }

    public function setItemID($itemID)
    {
        $this->itemID = $itemID;
    }

    public function getItemID()
    {
        return $this->itemID;
    }

    public function setDateReserve($date)
    {
        $this->dateReserve = $date;
    }

    public function getDateReserve()
    {
        return $this->dateReserve;
    }

    public function setDateApproved($dateApproved)
    {
        $this->dateApproved = $dateApproved;
    }

    public function getDateApproved()
    {
        return $this->dateApproved;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setPayment($payment)
    {
        $this->payment = $payment;
    }

    public function getPayment()
    {
        return $this->payment;
    }

    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    public function getArchive()
    {
        return $this->archive;
    }

    /**
     * @return mixed
     */
    public function getPickUpDate()
    {
        return $this->pickUpDate;
    }

    /**
     * @param mixed $pickUpDate
     */
    public function setPickUpDate($pickUpDate)
    {
        $this->pickUpDate = $pickUpDate;
    }

    /**
     * @return mixed
     */
    public function getPickOffDate()
    {
        return $this->pickOffDate;
    }

    /**
     * @param mixed $pickOffDate
     */
    public function setPickOffDate($pickOffDate)
    {
        $this->pickOffDate = $pickOffDate;
    }

    /**
     * @return mixed
     */
    public function getPickUpLocation()
    {
        return $this->pickUpLocation;
    }

    /**
     * @param mixed $pickUpLocation
     */
    public function setPickUpLocation($pickUpLocation)
    {
        $this->pickUpLocation = $pickUpLocation;
    }

    /**
     * @return mixed
     */
    public function getPassengerNo()
    {
        return $this->passengerNo;
    }

    /**
     * @param mixed $passengerNo
     */
    public function setPassengerNo($passengerNo)
    {
        $this->passengerNo = $passengerNo;
    }

    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param mixed $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }


}