<?php
// not yet tested
class File
{
    const TABLE_NAME = 'files';

    private $fileID;
    private $mime;
    private $bytes;
    private $dateAdded;
    private $remove;

    public static function LoadArray(array $ids, $includeRemoved = false)
    {
        $sql = 'SELECT 
                    file_id,
                    mime,
                    bytes,
                    date_added,
                    remove
                FROM
                    files
                WHERE
                    file_id IN (' . implode(', ', $ids) . ')';
        if (!$includeRemoved) {
            $sql .= ' AND remove = 0';
        }
        $result = DBcon::fetch_sql_assoc($sql, ['file_id']);
        if (!empty($result)) {
            $temp = [];
            foreach ($result as $key => $file) {
                $new = new static();
                $new->setFileID($file['file_id']);
                $new->setBytes($file['bytes']);
                $new->setMime($file['mime']);
                $new->setDateAdded($file['date_added']);
                $new->setRemove($file['remove']);
                $temp[$key] = $new;
            }
            return $temp;
        }
        return false;
    }

    public static function Load($id){
        $return = static::LoadArray([$id]);
        return reset($return);
    }

    public function save()
    {
        $data = [
            'mime' => $this->getMime(),
            'bytes' => $this->getBytes(),
            'remove' => $this->getRemove()
        ];
        if(!empty($this->getFileID())){
            $fileID = Dbcon::update('files',$data, [ 'file_id' => $this->getFileID() ]);
        }else{
            $fileID = DBcon::insert('files', $data);
        }
        return $fileID;
    }

    /**
     * @return mixed
     */
    public function getFileID()
    {
        return $this->fileID;
    }

    /**
     * @param mixed $fileID
     */
    public function setFileID($fileID)
    {
        $this->fileID = $fileID;
    }

    /**
     * @return mixed
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * @param mixed $mime
     */
    public function setMime($mime)
    {
        $this->mime = $mime;
    }

    /**
     * @return mixed
     */
    public function getBytes()
    {
        return $this->bytes;
    }

    /**
     * @param mixed $bytes
     */
    public function setBytes($bytes)
    {
        $this->bytes = $bytes;
    }

    /**
     * @return mixed
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * @param mixed $dateAdded
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
    }

    /**
     * @return mixed
     */
    public function getRemove()
    {
        return $this->remove;
    }

    /**
     * @param mixed $remove
     */
    public function setRemove($remove)
    {
        $this->remove = $remove;
    }
}