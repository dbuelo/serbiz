<?php

/**
 *  Item ORM
 *  handles database items
 *
 */

/**
 *  TODO
 *  Refactor the code for LoadItemByOwner and LoadItemToBeApproved
 *  This function should use the Load array instead to minimize the future sql repeatation
 *
 */
class Item
{
    const ITEM_TABLE = 'items';
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_DECLINED = 2;

    protected $itemID;
    protected $Item;
    protected $itemDescription;
    protected $itemPrice;
    protected $dateAdded;
    protected $brand;
    protected $model;
    protected $status;
    protected $ownersID;
    protected $images;
    protected $archive;
    protected $capacity;
    protected $brandNames = [];
    protected $address;
    protected $contact;


    public function __construct()
    {
        $this->brandNames = Brand::LoadArray();
    }

    public static function DisplayStatus($statusID)
    {
        $return = null;
        switch ($statusID) {
            case static::STATUS_PENDING:
                $return = 'Pending';
                break;
            case static::STATUS_DECLINED:
                $return = 'Declined';
                break;
            case static::STATUS_APPROVED;
                $return = 'Approved';
                break;
        }
        return $return;
    }

    public static function LoadArray($ids = [], $approved = false)
    {
        $sql = 'SELECT
                    item_id,
                    item_name,
                    item_description,
                    item_price,
                    date_added,
                    brand,
                    model,
                    images,
                    status,
                    owner_id,
                    archive,
                    capacity,
                    item_address,
                    item_contact_no
                FROM
                    ' . static::ITEM_TABLE . '
                WHERE
                    TRUE';
        if (count($ids) > 0) {
            $sql .= '
                AND
                    item_id IN (' . implode(', ', $ids) . ')
                ';
        }

        if ($approved) {
            $sql .= '
                AND
                    status = ' . static::STATUS_APPROVED;
        }

        $result = DBcon::execute($sql);
        $data = DBcon::fetch_all_assoc($result);
        $return = [];
        if (!empty($data)) {
            foreach ($data as $item) {
                $newItem = new static();
                $newItem->setItemID($item['item_id']);
                $newItem->setItemName($item['item_name']);
                $newItem->setItemDescription($item['item_description']);
                $newItem->setItemPrice($item['item_price']);
                $newItem->setDateAdded($item['date_added']);
                $newItem->setStatus($item['status']);
                $newItem->setBrand($item['brand']);
                $newItem->setOwnersID($item['owner_id']);
                $newItem->setImages($item['images']);
                $newItem->setModel($item['model']);
                $newItem->setArchive($item['archive']);
                $newItem->setCapacity($item['capacity']);
                $newItem->setAddress($item['item_address']);
                $newItem->setContact($item['item_contact_no']);
                $return[$item['item_id']] = $newItem;
            }
        }
        return $return;
    }

    public static function Load($id)
    {
        $return = static::LoadArray([$id]);
        if (!empty($return)) {
            return reset($return);
        } else {
            return false;
        }
    }

    public static function LoadItemByOwner($ownersID)
    {

        $sql = 'SELECT
                    item_id
                FROM
                    ' . static::ITEM_TABLE . '
                WHERE
                    TRUE
                AND
                    archive = 0';
        if (is_array($ownersID)) {
            $sql .= '
                AND
                    owner_id  IN (' . implode(', ', $ownersID) . ')
                ';
        } else {
            $sql .= '
                AND
                    owner_id  = ' . $ownersID;
        }
        $result = DBcon::execute($sql);
        $data = DBcon::fetch_all_assoc($result);
        $return = [];
        if (!empty($data)) {
            $itemIDs = [];
            foreach ($data as $item) {
                $itemIDs[] = $item['item_id'];
            }
            $return = static::LoadArray($itemIDs);
        }
        return $return;
    }

    public static function LoadItemToBeApprove($where = [])
    {
        $sql = 'SELECT
                    item_id,
                    item_name,
                    item_description,
                    item_price,
                    brand,
                    date_added,
                    status,
                    images,
                    owner_id
                FROM
                    ' . static::ITEM_TABLE . '
                WHERE
                    status = 0';
        if (count($where) > 0) {
            foreach ($where as $key => $value) {
                $sql .= "
                    AND
                        {$key} = '{$value}'
                    ";
            }
        }
        $result = DBcon::execute($sql);
        $data = DBcon::fetch_all_assoc($result);
        $return = [];
        if (!empty($data)) {
            foreach ($data as $item) {
                $newItem = new static();
                $newItem->setItemID($item['item_id']);
                $newItem->setItemName($item['item_name']);
                $newItem->setItemDescription($item['item_description']);
                $newItem->setItemPrice($item['item_price']);
                $newItem->setDateAdded($item['date_added']);
                $newItem->setStatus($item['status']);
                $newItem->setImages($item['images']);
                $newItem->setOwnersID($item['owner_id']);
                $return[$item['item_id']] = $newItem;
            }
        }
        return $return;
    }

    public function save()
    {
        if (isset($this->itemID)) {
            $where = ['item_id' => $this->itemID];
        }

        $data = [
            'item_name' => $this->itemName,
            'item_description' => $this->itemDescription,
            'item_price' => $this->itemPrice,
            'brand' => $this->brand,
            'model' => $this->model,
            'owner_id' => $this->ownersID,
            'images' => $this->images,
            'status' => $this->status,
            'archive' => $this->archive,
            'capacity' => $this->capacity
        ];

        if (isset($where)) {
            DBcon::update(static::ITEM_TABLE, $data, $where);
        } else {
            DBcon::insert(static::ITEM_TABLE, $data);
        }
    }


    public static function GetItemByOwner($ownerIDs)
    {
        $return = [];
        if (is_array($ownerIDs)) {
            foreach ($ownerIDs as $ownerID) {
                $sql = "SELECT item_id FROM " . static::ITEM_TABLE . " WHERE owner_id = " . $ownerID;
                $data = DBcon::execute($sql);
                $result = DBcon::fetch_assoc($data);
                Util::debug($result);
            }
        } else {
            $sql = "SELECT item_id FROM " . static::ITEM_TABLE . " WHERE owner_id = " . $ownerIDs;
            $data = DBcon::execute($sql);
            $result = DBcon::fetch_array($data);
            Util::debug($result);
            $return[$ownerIDs] = static::LoadArray($result);
        }
        return $return;
    }

    public static function ArchiveItem(int $itemID)
    {
        $where = ['item_id' => $itemID];
        DBcon::update(static::ITEM_TABLE, ['archive' => 1], $where);
    }

    public function getBrandName()
    {
        $brandList = $this->brandNames;
        if (!empty($brandList) && isset($brandList[$this->brand])) {
            $Brand = $brandList[$this->brand];
            return $Brand->getBrandName();
        }
        return 'n/a';
    }

    public function getOwnerName()
    {
        return User::getname($this->getOwnersID());
    }

    public function isApproved()
    {
        return $this->status;
    }

    public function isArchived()
    {
        return $this->archive;
    }

    /**
     *
     * @param int $itemID
     */
    public function setItemID($itemID)
    {
        $this->itemID = $itemID;
    }

    /**
     *
     * @return int
     */
    public function getItemID()
    {
        return $this->itemID;
    }

    /**
     *
     * @param string $itemName
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;
    }

    /**
     *
     * @return string
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     *
     * @param string $itemDescription
     */
    public function setItemDescription($itemDescription)
    {
        $this->itemDescription = $itemDescription;
    }

    /**
     *
     * @return string
     */
    public function getItemDescription()
    {
        return $this->itemDescription;
    }

    /**
     *
     * @param int $itemPrice
     */
    public function setItemPrice($itemPrice)
    {
        $this->itemPrice = $itemPrice;
    }

    /**
     *
     * @return int
     */
    public function getItemPrice()
    {
        return $this->itemPrice;
    }

    public function getItemPriceRange()
    {
        $priceRange = Util::formatCurrency($this->getItemPriceFrom()) . ' - ' . Util::formatCurrency($this->getItemPriceTo());
        return $priceRange;
    }

    public function getItemPriceFrom()
    {
        $price = explode('-', $this->itemPrice);
        return isset($price[0]) ? trim($price[0]) : '0';
    }

    public function getItemPriceTo()
    {
        $price = explode('-', $this->itemPrice);
        return isset($price[1]) ? trim($price[1]) : '0';
    }

    /**
     *
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     *
     * @param date/time $dateAdded
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
    }

    /**
     *
     * @return date/time
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    public function setOwnersID($ownersID)
    {
        $this->ownersID = $ownersID;
    }

    public function getOwnersID()
    {
        return $this->ownersID;
    }

    public function getOwnersDetails()
    {
        return User::Load($this->ownersID);
    }

    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    public function getArchive()
    {
        return $this->archive;
    }

    public function setImages($images)
    {
        $this->images = $images;
    }

    public function getImages()
    {
        $return = json_decode($this->images);
        return $return;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    private function getBrand()
    {
        return $this->brand;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getCapacity()
    {
        return $this->capacity;
    }

    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }
}