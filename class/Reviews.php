<?php

class Reviews
{
    const TABLE = 'reviews';

    protected $reviewID;
    protected $itemID;
    protected $userID;
    protected $rate;
    protected $name;
    protected $comment;
    protected $date;

    public static function Create()
    {
        $newReview = new static();
        $CurrenUser = User::GetCurrentUser();
        $newReview->setUserID($CurrenUser->getUserID());
        return $newReview;
    }

    public static function LoadArray($ids = [], $where = [])
    {
        $sql = 'SELECT
                    review_id,
                    item_id,
                    user_id,
                    rate,
                    name,
                    comment,
                    date
                FROM
                    ' . static::TABLE . '
                WHERE
                    TRUE';
        if (count($ids) > 0) {
            $sql .= '
                AND
                    review_id IN (' . implode(', ', $ids) . ')
                ';
        }

        if (!empty($where)) {
            foreach ($where as $key => $value) {
                $sql .= PHP_EOL . ' AND ' . $key . ' = ' . $value;
            }
        }
        $result = DBcon::execute($sql);
        $data = DBcon::fetch_all_assoc($result);
        $return = [];
        if (!empty($data)) {
            foreach ($data as $item) {
                $newReview = new static();
                $newReview->setReviewID($item['review_id']);
                $newReview->setUserID($item['user_id']);
                $newReview->setItemID($item['item_id']);
                $newReview->setRate($item['rate']);
                $newReview->setName($item['name']);
                $newReview->setComment($item['comment']);
                $newReview->setDate($item['date']);
                $return[$item['review_id']] = $newReview;
            }
        }
        return $return;
    }

    public static function Load($id)
    {
        $return = static::LoadArray([$id]);
        if (!empty($return)) {
            return reset($return);
        } else {
            return false;
        }
    }

    public static function GetAverageRate($itemID)
    {
        $sql ='SELECT
                    AVG(rate) as average
               FROM
                    ' . static::TABLE . '
               WHERE
                    item_id = ' . $itemID;
        $result = DBcon::execute($sql);
        $data = DBcon::fetch_assoc($result);
        $rate = round($data['average']);
        return $rate;
    }

    public function save()
    {
        $data = [
            'item_id' => $this->itemID,
            'user_id' => $this->userID,
            'rate' => $this->rate,
            'name' => $this->name,
            'comment' => $this->comment
        ];
        if (empty($this->reviewID)) {
            $result = DBcon::insert(static::TABLE, $data);
        } else {
            $where = ['review_id' => $this->reviewID];
            $result = DBcon::update(static::TABLE, $data, $where);
        }
    }

    public function getUsername()
    {
        $username = User::getName($this->userID);
        $return = !empty($username) ? $username : 'n/a';
        return $return;
    }

    public function LoadReviewsByItem($id)
    {
        $where = ['item_id' => $id];
        $data = static::LoadArray([], $where);
        return $data;
    }

    public function setReviewID($reviewID)
    {
        $this->reviewID = $reviewID;
    }

    public function getReviewID()
    {
        return $this->reviewID;
    }

    public function setItemID($itemID)
    {
        $this->itemID = $itemID;
    }

    public function getItemID()
    {
        return $this->itemID;
    }

    public function setUserID($userID)
    {
        $this->userID = $userID;
    }

    public function getUserID()
    {
        return $this->userID;
    }

    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    public function getRate()
    {
        return $this->rate;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    public function getComment()
    {
        return $this->comment;
    }

    private function setDate($date)
    {
        $this->date = $date;
    }

    public function getDate()
    {
        return $this->date;
    }
}