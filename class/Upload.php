<?php

class Upload extends UploadInterface
{

    /**
     * Decodes sizes
     *
     * @access private
     * @param  string  $size  Size in bytes, or shorthand byte options
     * @return integer Size in bytes
     */
    function getsize($size)
    {
        if ($size === null) {
            return null;
        }
        $last = strtolower(substr($size, -1));
        $size = (int) $size;
        switch ($last) {
            case 'g':
                $size *= 1024;
            case 'm':
                $size *= 1024;
            case 'k':
                $size *= 1024;
        }
        return $size;
    }
}