<?php

//databaseConnection

class   Dbcon
{
    //Tables
    const TABLE_TENANT_RESERVATION = 'tenant_reservation';
    const TABLE_COMMENTS = 'comments';
    const TABLE_PROPERTY = 'tbl_property';
    const TABLE_USERS = 'tbl_users';

    //define the variable needed
    public static $host = '127.0.0.1';
    public static $username = 'root';
    public static $password = '';
    public static $dbname = 'serbiz';
    public static $conn;
    static $error;

    //Connect to the database
    public static function connect()
    {
        try {
            self::$conn = mysqli_connect(self::$host, self::$username, self::$password, self::$dbname);
        } catch (Exception $e) {
            echo "<pre>" . $e . "</pre>";
        }
        return static::$conn;
    }

    //select data from the table
    function select($table, $data = null, $value = null)
    {
        $this->connect();
        $value = $this->clean($value);
        if ($value !== "" and $data !== "") {
            $query = "SELECT * FROM $table WHERE $data=$value";
        } else {
            $query = "SELECT * FROM $table";
        }
        if ($result = mysqli_query($this->conn, $query)) {
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return "error";
        }
        $this->close();
    }

    public static function update($table, array $data, array $where)
    {
        $sql = "UPDATE {$table} SET ";
        $x = 1;
        foreach ($data as $key => $value) {
            $valueCleaned = static::clean($value);
            if ($x === count($data)) {
                $sql .= " {$key} = '{$valueCleaned}' ";
            } else {
                $sql .= " {$key} = '{$valueCleaned}', ";
            }
            $x++;
        }
        $sql .= "WHERE true ";
        foreach ($where as $key => $value) {
            $cleanedValue = static::clean($value);
            $sql .= " AND {$key} = '{$cleanedValue}'";
        }
        self::execute($sql);
        return mysqli_affected_rows(self::$conn);
    }

    /**
     *  Insert data into
     * @param $table name of table
     * @param $data
     * @return bool|int|string
     */
    public static function insert($table, $data)
    {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if(is_array($value)){
                   $temp =[];
                   foreach($value as $arrayData){
                       $temp[] = static::clean($arrayData);
                   }
                   $cleanData[$key] = json_encode($temp);
                }else{
                  $cleanData[$key] = static::clean($value);
                }
            }
        }
        $fields = implode(',', array_keys($cleanData));
        $data = implode("','", $cleanData);
        $query = "INSERT INTO {$table} ({$fields}) VALUES ('{$data}')";
        try {
            self::connect();
            mysqli_query(self::$conn, $query);
            return mysqli_insert_id(self::$conn);
        } catch (Exception $e) {
            self::$error = mysqli_error(static::$conn);
            return false;
        }
    }

    public static function execute($query)
    {
        self::connect();
        if ($result = mysqli_query(self::$conn, $query)) {
            return $result;
        } else {
            self::$error = mysqli_error(self::$conn);
            return false;
        }
    }

    public static function fetch_all_assoc($object, $keys = null)
    {
        //handle database object
        if (!empty($object)) {
            $result = mysqli_fetch_all($object, MYSQLI_ASSOC);
            mysqli_free_result($object);
            $tmp = [];
            static::close();
            if (is_array($keys)) {
                foreach ($keys as $key => $value) {
                    foreach ($result as $res) {
                        $tmp[$res[$value]] = $res;
                    }
                }
                return $tmp;
            } else {
                return $result;
            }
        }
    }

    public static function fetch_all_array($object)
    {
        //handle database object
        if (!empty($object)) {
            $result = mysqli_fetch_all($object, MYSQLI_NUM);
            mysqli_free_result($object);
            static::close();
            return $result;
        }
    }

    public static function fetch_array($object)
    {
        //handle database object
        if (!empty($object)) {
            $result = mysqli_fetch_array($object, MYSQLI_NUM);
            mysqli_free_result($object);
            static::close();
            return $result;
        }
    }

    public static function fetch_sql_assoc($sql,$key = null)
    {
       $result = static::execute($sql);
       $data =  static::fetch_all_assoc($result,$key);
       return $data;
    }

    public static function fetch_assoc($object)
    {
        //handle database object
        if (!empty($object)) {
            $result = mysqli_fetch_assoc($object);
            mysqli_free_result($object);
            self::close();
            return $result;
        }
    }

    public function fetch_row($object)
    {
        if (!empty($object)) {
            $result = mysqli_fetch_row($object);
            mysqli_free_result($object);
            self::close();
            return $result;
        }
    }

    //delete the file from database
    public static function delete($table, $where)
    {
        $query = "
            DELETE FROM 
                {$table} 
            WHERE
                TRUE
        ";
        foreach ($where as $key => $value) {
            $query .= "
                AND 
                    {$key} = '{$value}'
            ";
        }
        self::execute($query);
    }

    //clean the data before posting it to the data base
    public static function clean($x)
    {
        self::connect();
        if ($x <> null) {
            try{
                $x = stripcslashes($x);
                $x = mysqli_real_escape_string(self::$conn, $x);
                return $x;}
            catch (Exception $e){
                return $x;
            }
        } else {
            return false;
        }
    }

    public static function close()
    {
        mysqli_close(self::$conn);
    }

    //destroy the connection everytime the page is closed
    function __destruct()
    {
        mysqli_close($this->conn);
    }

    public static function fetch_num_rows($sql)
    {
        $result = self::execute($sql);
        $resultCheck = mysqli_num_rows($result);
        return $resultCheck;
    }

    function page_selectAll($offset = 1, $rowsperpage = 1)
    {
        $query = 'SELECT * FROM content LIMIT 3 OFFSET 0';
        $data = mysqli_fetch_assoc($this->execute($query));
        return $data;
    }
}