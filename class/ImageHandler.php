<?php

/**
 * Class ImageHandler
 */


class ImageHandler
{
    private static $acceptedMime = [
        'image/jpeg' => 'jpg',
        'image/png' => 'png',
        'image/gif' => 'gif'
    ];

    /**
     * @param $path
     * @param $mime
     * @return string value of the image in base64
     */
    public static function GetImageRaw($path, $mime)
    {
        if (isset(static::$acceptedMime[$mime])) {
            $imageData = file_get_contents($path);
            return base64_encode($imageData);

        }
    }

    /**
     * @param $base64Data
     * @param $mime
     * @return  string
     */
    public static function ReconstructImage($base64Data, $mime)
    {

        if (isset(static::$acceptedMime[$mime])) {
            return 'data:' . $mime . ';charset=utf-8;base64,' . $base64Data;
        }
        return false;
    }
}