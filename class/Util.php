<?php

class Util
{

    public static function redirect($url)
    {
        ob_start();
        header('Location:' . $url);
        ob_end_flush();
    }

    public static function getParam($param)
    {
        if (isset($_REQUEST[$param])) {
            return $_REQUEST[$param];
        } else {
            return false;
        }
    }

    public static function date($date = null)
    {
        if (!empty($date)) {
            $formated = date('m/d/Y', strtotime($date));
        } else {
            $formated = date('m/d/Y');
        }
        return $formated;
    }

    public static function Debug($data, $title = null)
    {
        if (!empty($title)) {
            echo "<strong>{$title}</strong>";
        }
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }

    public static function BackTrace()
    {
        $traces = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 5);
        $tmp = [];
        foreach (array_slice($traces, 1) as $trace) {
            $tmp[] = 'Line: ' . $trace['line'] . ' - ' . $trace['function'] . '() ' . $trace['file'];
        }
        static::Debug($tmp);
    }

    public static function formatCurrency($amount)
    {
        $return = '₱ ' . number_format($amount, 2);
        return $return;
    }

    public static function CreateDialog($title, $message)
    {
        ?>
        <div class="modal show">
            <div class="modalContainer small-modal">
                <div class="header">
                    <?= $title ?>
                    <div class="close">x</div>
                </div>
                <div class="content small-modal">
                    <div class="row">
                        <div class="col-md-12 details">
                            <?= $message ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    /**
     *
     * @param string $text
     * @param int $size - size of the character 
     *
     * @return string 
     */
    public static function WrapText($text, $size = 10)
    {
        $sizeOfText = strlen($text);
        if ($sizeOfText > $size) {
            $text = substr($text, 0, $size);
            $text .= '...';
            //let us check for the word completion
        }
        return $text;
    }
}