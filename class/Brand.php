<?php

class Brand
{
    const TABLE_NAME = 'brands';

    protected $brandID;
    protected $brandName;
    protected $archive;

    public static function LoadArray($ids = [], $archive = true)
    {
        $sql = 'SELECT
                   brand_id,
                   brand_name,
                   archive
                FROM
                   ' . static::TABLE_NAME . '
                WHERE
                   TRUE';
        if (count($ids) > 0) {
            $sql .= '
                AND
                    brand_id IN (' . implode(", ", $ids) . ')';
        }
        if ($archive === true) {
            $sql .= '
                AND
                    archive = 0;
                ';
        }


        $Obj = DBcon::execute($sql);
        $data = DBcon::fetch_all_assoc($Obj);
        $return = [];

        if (!empty($data) > 0) {
            foreach ($data as $brand) {
                $new = new static();
                $new->setBrandID($brand['brand_id']);
                $new->setBrandName($brand['brand_name']);
                $new->setArchive($brand['archive']);
                $return[$brand['brand_id']] = $new;
            }
        }
        return $return;
    }

    public static function Load($id)
    {
        $return = static::LoadArray([$id]);
        if (!empty($return)) {
            $return = reset($return);
        }
        return $return;
    }

    public function save()
    {
        // TODO:
        // no code
    }

    /**
     * @param mixed $brandID
     */
    public function setBrandID($brandID)
    {
        $this->brandID = $brandID;
    }

    /**
     * @param mixed $brandName
     */
    public function setBrandName($brandName)
    {
        $this->brandName = $brandName;
    }

    /**
     * @param mixed $archive
     */
    public function setArchive($archive)
    {
        $this->archive = $archive;
    }

    /**
     * @return mixed
     */
    public function getBrandName()
    {
        return $this->brandName;
    }

    /**
     * @return mixed
     */
    public function getBrandID()
    {
        return $this->brandID;
    }

    /**
     * @return mixed
     */
    public function getArchive()
    {
        return $this->archive;
    }
}