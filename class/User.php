<?php

class User
{
    const TABLE_NAME = 'tbl_users';
    const TABLE_TENANT_RESERVATION = 'tenant_reservation';
    const ADMIN = 5;
    const OWNER = 1;
    const TENANT = 0;

    protected $userID;
    protected $userName;
    protected $userFname;
    protected $userContact;
    protected $userEmail;
    protected $confirm;
    protected $userType;
    protected $facebookID;
    protected $attachment;
    protected $role;
    protected $profile;

    protected $contactNo;
    protected $address;

    public static function GetCurrentUser()
    {
        if (isset($_SESSION['User'])) {
            $User = unserialize($_SESSION['User']);
            return $User;
        }
        return false;
    }

    public static function LoadArray($ids = [])
    {
        // attach, profile_img,
        $sql = 'SELECT
                    user_id,
                    user_name,
                    user_pass,
                    user_fname,
                    user_contact,
                    user_email,
                    attach,
                    confirm,
                    role,
                    profile_img,
                    address,
                    contact_no
               FROM
                    ' . static::TABLE_NAME . '
               WHERE
                    TRUE';
        if (count($ids) > 0) {
            $sql .= '
                AND
                    user_id IN (' . implode(", ", $ids) . ')';
        }
        $object = DBcon::execute($sql);
        $result = DBcon::fetch_all_assoc($object, 'user_id');
        $return = [];
        if (!empty($result)) {
            foreach ($result as $data) {
                $new = new self();
                $new->setUserID($data['user_id']);
                $new->setUserName($data['user_name']);
                $new->setUserFname($data['user_fname']);
                $new->setUserContact($data['user_contact']);
                $new->setUserEmail($data['user_email']);
                $new->setConfirm($data['confirm']);
                $new->setRole($data['role']);
                $new->setAttachment($data['attach']);
                $new->setProfileImage($data['profile_img']);
                $new->setContactNo($data['contact_no']);
                $new->setAddress($data['address']);
                $return[$data['user_id']] = $new;
            }
        }
        return $return;
    }

    public static function Load($userID)
    {
        $return = static::LoadArray([$userID]);
        if (!empty($return)) {
            $return = reset($return);
        }
        return $return;
    }

    public static function getEmailAddress($userID)
    {
        $sql = "
            SELECT
                user_email
            FROM
                " . static::TABLE_NAME . "
            WHERE
                user_id = " . $userID;
        $result = DBcon::execute($sql);
        $data = DBcon::fetch_array($result);
        if (!empty($data)) {
            return $data[0];
        } else {
            return false;
        }

    }

    public static function ifApproved($userID, $propertyID)
    {
        $sql = "
            SELECT
                approve
            FROM
                " . self::TABLE_TENANT_RESERVATION . "
            WHERE
                user_id = {$userID}
            AND
                tbl_property_id = {$propertyID}
        ";
        $result = DBcon::execute($sql);
        $data = DBcon::fetch_array($result);
        if (!empty($data[0])) {
            return true;
        }
        return false;
    }

    public static function getName($userID)
    {
        $sql = "
            SELECT
                user_fname
            FROM
                " . static::TABLE_NAME . "
            WHERE
                user_id = " . $userID;
        $result = DBcon::execute($sql);
        $data = DBcon::fetch_assoc($result);
        if (!empty($data)) {
            return $data['user_fname'];
        }
        return null;
    }

    public static function GetUserole($userID)
    {
        $sql = "
            SELECT
                role
            FROM
                " . self::TABLE_NAME . "
            WHERE
                user_id = {$userID}
        ";
        $result = DBcon::execute($sql);
        $data = DBcon::fetch_array($result);
        return $data[0];
    }

    public static function FindName($data)
    {
        $sql = "SELECT user_id from " . static::TABLE_NAME . " WHERE user_fname='{$data}'";
        $result = DBcon::execute($sql);
        $data = DBcon::fetch_assoc($result);
        if (!empty($data)) {
            return $data['user_id'];
        } else {
            return false;
        }
    }

    public static function FindEmail($data)
    {
        $sql = "SELECT user_id from " . static::TABLE_NAME . " WHERE user_email='{$data}'";
        $result = DBcon::execute($sql);
        $data = DBcon::fetch_assoc($result);
        if (!empty($data)) {
            return $data['user_id'];
        } else {
            return false;
        }
    }

    public static function ifReserved($ownerID, $tblPropertyID, $roomDetailsID = null)
    {
        if (isset($_SESSION['user_id'])) {
            $sql = "
            SELECT
                user_id
            FROM
                tenant_reservation
            WHERE
                user_id = {$_SESSION['user_id']}
            AND
                owner_id = {$ownerID}
            AND
                tbl_property_id = {$tblPropertyID}
        ";
            if (!empty($roomDetailsID)) {
                $sql .= "
                AND
                    room_details_id = {$roomDetailsID}
            ";
            }
            return Dbcon::fetch_num_rows($sql);
        }
        return false;
    }

    public function getRoleName()
    {
        switch ($this->role) {
            case static::ADMIN:
                $role = 'Admin';
                break;
            case static::TENANT:
                $role = 'Tenant';
                break;
            case static::OWNER:
                $role = 'Operator';
                break;
        }
        return $role;
    }

    public function setAttachment($attach)
    {
        $this->attachment = $attach;
    }

    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userID
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
    }

    /**
     * @return mixed
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @param mixed $confirm
     */
    public function setConfirm($confirm)
    {
        $this->confirm = $confirm;
    }

    /**
     * @return mixed
     */
    public function getConfirm()
    {
        return $this->confirm;
    }

    public function isConfirmed(){
        return ($this->confirm == 1) ? true : false;
    }
    /**
     * @param mixed $userEmail
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;
    }

    /**
     * @return mixed
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * @param mixed $userContact
     */
    public function setUserContact($userContact)
    {
        $this->userContact = $userContact;
    }

    /**
     * @return mixed
     */
    public function getUserContact()
    {
        return $this->userContact;
    }

    /**
     * @param mixed $userFname
     */
    public function setUserFname($userFname)
    {
        $this->userFname = $userFname;
    }

    /**
     * @return mixed
     */
    public function getUserFname()
    {
        return $this->userFname;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    public function setProfileImage($profile)
    {
        $this->profile = $profile;
    }

    public function getProfileImage()
    {
        return $this->profile;
    }

    /**
     * @return mixed
     */
    public function getContactNo()
    {
        return $this->contactNo;
    }

    /**
     * @param mixed $contactNo
     */
    public function setContactNo($contactNo)
    {
        $this->contactNo = $contactNo;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
}
//EOF