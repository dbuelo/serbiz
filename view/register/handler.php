<?php
$username = Util::getParam('username');
$password = Util::getParam('pass');
$role = Util::getParam('role');
$fullName = Util::getParam('fullName');
$address = Util::getParam('address');
$contactNo = Util::getParam('contactNo');

if (!empty($username) && !empty($password)) {
    $fname = Util::getParam('fname');
    $sql = "SELECT user_id FROM tbl_users WHERE user_email='$username'";
    $result = DBcon::execute($sql);
    $resultCheck = mysqli_num_rows($result);
    if ($resultCheck < 1) {
        if (isset($_FILES['profileImage'])) {
            $path = 'upload/';
            $ImageUpload = new Upload($_FILES['profileImage']);
            $ImageUpload->file_new_name_body = "attach_";
            if ($ImageUpload->uploaded) {
                $ImageUpload->Process($path);
                if ($ImageUpload->processed) {
                    //upload success
                    $fileName = ($ImageUpload->file_dst_name);
                } else {
                    echo 'error : ' . $ImageUpload->log;
                    die($ImageUpload->log);
                }
            }
        }
        $attach = isset($fileName) ? $fileName : '';

        $data = [
            'user_email' => $username,
            'user_fname' => $fullName,
            'user_pass' => md5($password),
            'contact_no' => $contactNo,
            'address' => $address,
            'role' => $role,
            'attach' => $attach
        ];

        DBcon::insert(User::TABLE_NAME, $data);
        $_SESSION['message'] = ['title' => 'Success', 'message' => 'Your account was successfully registered. Please wait for the admin to approve your account'];
        Util::redirect('index.php');
    } else {
        $redirect = '<a href="register.php">Try Again</a>';
        die('Error: Email Already Exist   ' . $redirect);
    }
}
