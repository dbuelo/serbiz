<div class="formContainer">
    <div class="row top-buffer">
        &nbsp;
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="login-form col-md-6 col-md-offset-3">
                <?php
                if (isset($message) && !empty($message)) {
                    echo '<p class="text-center alert-message">' . $message . "</p>";
                }
                ?>
                <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                    <h2 class="text-center">Registration</h2>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="username" class="form-control custom-input" placeholder="Email"
                                   required="required" autocomplete="off">
                        </div>
                        <div class="form-group">

                            <label>Password</label>
                            <input type="password" name="pass" id='pass1' class="form-control custom-input"
                                   placeholder="Password" required="required" autocomplete="off">
                            <span id='passwordVerification' class='text-danger'></span>
                        </div>
                        <div class="form-group">
                            <label>Retype Password</label>
                            <input type="password" name="pass" id='pass2' class="form-control custom-input"
                                   placeholder="Re-Password" required="required" autocomplete="off">
                            <span id='passwordMatch' class='text-danger'></span>
                        </div>


                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Full Name</label>
                            <input type="text" name="fullName" class="form-control custom-input" placeholder="Full Name"
                                   required="required" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" name="address" class="form-control custom-input" placeholder="Address"
                                   required="required" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Contact No.</label>
                            <input type="text" name="contactNo" class="form-control custom-input"
                                   placeholder="Contact No."
                                   required="required" autocomplete="off">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group text-center">
                                <label class="radio-inline"><input type="radio" name="role" value="1">Operator</label>
                                <label class="radio-inline"><input type="radio" name="role" value="0"
                                                                   checked>Renter</label>
                            </div>
                            <div class="form-group" style="display: none">
                                <label>Attach a valid ID:</label>
                                <input class="" type="file" name="profileImage">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary btn-block w-25" id="submit" type="submit">Submit</button>
                            </div>
                            <p class="text-center"><a href="login.php" class="text-info">Already have an account</a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
