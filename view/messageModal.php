<!-- MODAL -->
<?php
$UserID = $User->getUserID();
?>
<div class="modal modalShow fade-in">
    <div class="modalBox">
        <form action="processMessage.php" method="post" id="newMessageForm">
            <input type="hidden" name="sender" value="<?= $UserID ?>">
            <input type="text" name="to" class="to" placeholder="Name" value="" list="users">
            <div><textarea class="message" name="message" placeholder="Message"></textarea></div>
            <div class="inlineButton right"><button type="button" class="btn btn-danger modalTriggerHide">Cancel</button><button type="submit" class="btn btn-primary">Send</button>
            </div>
        </form>
    </div>
</div>
<datalist id="users">
    <?php
    $users = User::LoadArray();
    if (!empty($users)) {
        $html = '';
        foreach ($users as $User) {
            $html .= '<option value="' . $User->getUserEmail() . '">' . PHP_EOL;
        }
        echo $html;
    }
    ?>
</datalist>
