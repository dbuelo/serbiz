<?php
$Item = Item::Load($id);
$_SESSION['Item'][$id] = serialize($Item);
$images = $Item->getImages();
$imageToDisplay = null;
if (count($images) > 0) {
    $imageToDisplay = $images[0];
}
?>
<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
    <input type="hidden" name="task" value="saveEdit">
    <input type="hidden" name="itemID" value="<?= $Item->getItemID() ?>">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12" id="imageHolder">
                        <?php
                        if (!empty($imageToDisplay)) {
                            echo '<img src="upload/' . $imageToDisplay . '">';
                        } else {
                            echo '<img src="upload/defaults/imagePreview.jpg">';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Edit Item : "<?= $Item->getItemName() ?>"</h3>
                    </div>
                    <div class="col-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="itemName">Item Name : </label>
                                <input type="text" class="form-control" id="itemName" name="itemName"
                                       placeholder="Item Name" value="<?= $Item->getItemName() ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="itemDescription">Item Description : </label>
                                <textarea type="text" name="itemDescription" class="form-control" id="itemDescription"
                                          placeholder="Item Description" rows="8 "
                                          style="resize: none;"><?= $Item->getItemDescription() ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="brand">Brand : </label>
                                <select type="text" class="form-control" id="brand" name="brand">
                                    <option value="" disabled selected>Select A Brand</option>
                                    <?php
                                    $Brands = Brand::LoadArray();
                                    if (!empty($Brands)) {
                                        $html = null;
                                        $currentBrand = $Item->getBrandName();
                                        foreach ($Brands as $Brand) {
                                            $name = $Brand->getBrandName();
                                            $id = $Brand->getBrandID();
                                            if ($name === $currentBrand) {
                                                $html .= '<option value="' . $id . '" selected>' . $name . '</option>';
                                            } else {
                                                $html .= '<option value="' . $id . '">' . $name . '</option>';
                                            }
                                        }
                                        print($html);
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="itemPrice">Price : </label>
                                <div class="row">
                                    <div class="col-md-5">
                                        <input type="number" class="form-control col-6" id="itemPriceFrom"
                                               name="itemPriceFrom" placeholder="Item Price From"
                                               value="<?= $Item->getItemPriceFrom()?>" required>
                                    </div>
                                    <div class="col-md-2">to</div>
                                    <div class="col-md-5">
                                        <input type="number" class="form-control col-6" id="itemPriceTo"
                                               name="itemPriceTo" placeholder="Item Price To"
                                               value="<?= $Item->getItemPriceTo()?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="itemModel">Model : </label>
                                <input type="text" class="form-control" id="itemModel" name="itemModel"
                                       placeholder="Model" value="<?= $Item->getModel() ?>">
                            </div>
                            <div class="form-group">
                                <label for="itemModel">Capacity : </label>
                                <input type="number" class="form-control" id="itemCapacity" name="capacity"
                                       placeholder="Capacity" value="<?= $Item->getCapacity() ?>">
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4 right float-right">

                                <button type="button" class="btn btn-primary" onclick="window.history.back();">Cancel
                                </button>
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

