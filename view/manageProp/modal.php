<?php

foreach ($propertyList as $Item) {
    ?>

    <div class="modal" id="item_<?= $Item->getItemID() ?>">
        <div class="modalContainer">
            <div class="header">
                <?= $Item->getItemName(); ?> Information
                <div class="close">x</div>
            </div>
            <div class="content">
                <div class="col-md-12">
                    <div id="wowslider-container1">
                        <div class="ws_images noborder" style="border:1px solid black"><ul>
                                <?php
                                $images = $Item->getImages();
                                if (!empty($images)) {
                                    $html = null;
                                    foreach ($images as $image) {
                                        $html .= ' <li><img class="wowSliderImage" src="upload/' . $image . '" alt="slider html" title="ws_Futuristic_Saying_1366x768" id="wows1_0"/></a></li>';
                                    }
                                }
                                print $html;
                                ?>

                            </ul></div>

                        <div class="ws_shadow"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 details">
                            <strong>Item Name: </strong>
                        </div>
                        <div class="col-md-7 details">
                            <?= $Item->getItemName() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 details">
                            <strong>Item Description: </strong>
                        </div>
                        <div class="col-md-7 details">
                            <?= $Item->getItemDescription() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 details">
                            <strong>Item Price: </strong>
                        </div>
                        <div class="col-md-7 details">
                            <?= Util::formatCurrency($Item->getItemPrice()) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 details">
                            <strong>Date Added: </strong>
                        </div>
                        <div class="col-md-7 details">
                            <?= Util::date($Item->getDateAdded()) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 details">
                            <strong>Brand: </strong>
                        </div>
                        <div class="col-md-7 details">
                            <?= $Item->getBrandName() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 details">
                            <strong>Model: </strong>
                        </div>
                        <div class="col-md-7 details">
                            <?= $Item->getModel() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 details">
                            <strong> Owner: </strong>
                        </div>
                        <div class="col-md-7 details">
                            <?= $Item->getOwnerName() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 details">
                            <strong> Status: </strong>
                        </div>
                        <div class="col-md-7 details">
                            <?= Item::DisplayStatus($Item->getStatus()) ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <?php
}
?>