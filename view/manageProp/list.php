<?php
$propertyList = Item::LoadItemByOwner($userid);
?>

<div class="row">
    <div class="col-md-12 myPropList">
        <?php
        if (!empty($propertyList)) {
            $html = null;
           
            foreach ($propertyList as $Item) {
                $images = $Item->getImages();
                $imageToDisplay = !empty($images) ? $images[0] : 'defaults/imagePreview.jpg';
                if (!file_exists('upload/' . $imageToDisplay)) {
                    $imageToDisplay = 'defaults/imagePreview.jpg';
                }
                $ItemName = !empty($Item->getItemName()) ? $Item->getItemName() : "Item Name Not Found";
                $html .= '<div class="col-md-3">';
                $html .= '<div class="card bg-success myPropContainer">';
                $html .= '<div class="card-header"><strong data-toggle="tooltip" data-placement="top" title="' . $Item->getItemName() . '">';
                $html .= Util::WrapText($ItemName,25);
                $html .= '</strong></div>';
                $html .= '<div class="card-body body">';
                $html .= '<span style="display:block;text-align:center;" class="image-container"><img src="upload/' . $imageToDisplay . '" ></span>';
                $html .= '<div class="col-md-12 text-center buttons"><button class="btn btn-primary" data-id="' . $Item->getItemID() . '">Details</button>  <a class="btn btn-primary" style="color:white" href="manageProp.php?task=edit&id=' . $Item->getItemID() . '">Edit</a> <a class="btn btn-danger" style="color:white" href="manageProp.php?task=delete&itemID=' . $Item->getItemID() . '">Delete</a></div></div>';
                $html .= '</div></div>';
            }
            print $html;
        } else {
            ?>
            <div class="col-md-12 text-center">
                No Properties listed
            </div>
            <?php
        }
        ?>
    </div>
</div>
