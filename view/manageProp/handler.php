<?php
$task = Util::getParam('task');
$itemID = Util::getParam('itemID');

switch ($task) {
    case 'saveEdit':
        saveEdit($itemID);
        break;
    case 'delete':
        Item::ArchiveItem($itemID);
        break;
}

function saveEdit($itemID)
{
    if (isset($_SESSION['Item'][$itemID])) {
        $Item = unserialize($_SESSION['Item'][$itemID]);
        //varianbles
        $itemName = Util::getParam('itemName');
        $itemDescription = Util::getParam('itemDescription');
        $brand = Util::getParam('brand');
        $itemPriceTo = Util::getParam('itemPriceTo');
        $itemPriceFrom = Util::getParam('itemPriceFrom');
        $itemModel = Util::getParam('itemModel');
        $capacity = Util::getParam('capacity');
        $itemPrice = $itemPriceFrom . ' - ' . $itemPriceTo;
        Util::debug($itemPrice);
        $Item->setItemName($itemName);
        $Item->setItemDescription($itemDescription);
        $Item->setBrand($brand);
        $Item->setItemPrice($itemPrice);
        $Item->setModel($itemModel);
        $Item->setCapacity($capacity);
        $Item->save();
        Util::CreateDialog('Change save', 'Successfully save item ' . $Item->getItemName());
        unset($_SESSION['Item'][$itemID]);
    }
}
