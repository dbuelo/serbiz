<?php
$actionButton = Util::getparam('approve');
$reservationID = Util::getparam('rid');
$itemID = Util::getparam('itemID');
$dateReserve = Util::getparam('dateReserve');
if (!empty($actionButton)) {
    switch ($actionButton) {
        case 'Approve':
            Reservation::ChangeStatus($reservationID, Reservation::STATUS_APPROVED, $itemID, $dateReserve);
            break;
        case 'Decline':
            Reservation::ChangeStatus($reservationID, Reservation::STATUS_DECLINE, $itemID, $dateReserve);
            break;
        case 'Pending':
            Reservation::ChangeStatus($reservationID, Reservation::STATUS_PENDING, $itemID, $dateReserve);
            break;
    }
}