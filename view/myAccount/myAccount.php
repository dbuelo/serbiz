<?php
$CurrentUser = User::GetCurrentUser();

$sql  = 'SELECT
            i.item_id,
            r.reservation_id
         FROM
            reservations r
         LEFT JOIN
            items i
         USING
            (item_id)
         WHERE
            i.owner_id = '. $CurrentUser->getUserID() . '
         AND
            r.status IN (0, 4)';
$Obj = DBcon::execute($sql);
$items = DBcon::fetch_all_assoc($Obj);
if (!empty($items)) {
    $ids = [];
    foreach ($items as $item) {
        $ids[] = $item['reservation_id'];
    }
    $Reservations = Reservation::LoadArray($ids);
}
?>
<div class="row">
    <div class="line">
        <div class="box margin-bottom">
            <div class="col-md-12">
                <div class="s-12 m-12 l-12 xl-12 xxl-12 center ">
                    <h3>Reservation</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="tspots_all" class="table table-striped table-bordered table-sm" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Item Name</th>
                                    <th>Renter Name</th>
                                    <th>Renter Contact</th>
                                    <th>Pick up date</th>
                                    <th>Pick off Date</th>
                                    <th>Passenger No.</th>
                                    <th>Pick up location</th>
                                    <th>Note</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($Reservations)) {
                                    foreach ($Reservations as $Reservation) {
                                        $Item = $Reservation->getItemDetails();
                                        $RenterDetails = $Reservation->getRenterDetails();

                                        ?>
                                    <form method="post" action="<?= $_SERVER['PHP_SELF']; ?>">
                                        <input type="hidden" name="rid" value="<?= $Reservation->getReservationID(); ?>">
                                        <input type="hidden" name="dateReserve" value="<?= $Reservation->getDateReserve(); ?>">
                                        <input type="hidden" name="itemID" value="<?= $Reservation->getItemID(); ?>">
                                        <tr>
                                            <td><?= $Item->getItemName(); ?></td>
                                            <td><?= $RenterDetails->getUserFname(); ?></td>
                                            <td><?= $RenterDetails->getContactNo(); ?></td>
                                            <td><?= Util::date($Reservation->getPickUpDate()); ?></td>
                                            <td><?= Util::date($Reservation->getPickOffDate()); ?></td>
                                            <td><?= $Reservation->getPassengerNo(); ?></td>
                                            <td><?= $Reservation->getPickUpLocation(); ?></td>
                                            <td><?= $Reservation->getNotes(); ?></td>
                                            <td>
                                                <input type="submit"  name="approve" class="btn btn-success" value="Approve">
                                                <input type="submit"  name="approve" class="btn btn-primary" value="Pending">
                                                <input type="submit"  name="approve" class="btn btn-danger" value="Decline">
                                            </td>
                                        </tr>
                                    </form>
                                    <?php
                                     }
                                }else{
                                    ?>
                                    <tr>
                                        <td colspan="9" class="text-center">No Reservations Found </td>
                                    </tr>
                                    <?php
                                }
                           
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>