<?php
// will handle the posting of Items
if (isset($_POST['action'])) {
    $path = 'upload/';
    $itemName = Util::getParam('itemName');
    $itemDescription = Util::getParam('itemDescription');
    $brand = Util::getParam('brand');
    $itemPrice = Util::getParam('itemPrice');
    $model = Util::getParam('itemModel');
    $ownerID = $User->getUserID();
    $capacity = Util::getParam('capacity');
    $itemPriceTo = Util::getParam('itemPriceTo');
    $itemPriceFrom = Util::getParam('itemPriceFrom');
    $itemPrice = $itemPriceFrom . ' - ' . $itemPriceTo;
    $fileName = [];
    $files = [];
    if (isset($_FILES['image'])) {
        $counter = 0;
        foreach ($_FILES['image'] as $key => $images) {
            foreach ($images as $image) {
                $files[$counter][$key] = $image;
                $counter++;
            }
            $counter = 0;
        }
        if (!empty($files)) {
            foreach ($files as $file) {
                $ImageUpload = new Upload($file);
                $ImageUpload->file_new_name_body = "serbiz_";
                if ($ImageUpload->uploaded) {
                    $ImageUpload->Process($path);
                    if ($ImageUpload->processed) {
                        //upload success
                        $fileName[] = ($ImageUpload->file_dst_name);
                    } else {
                        echo 'error : ' . $ImageUpload->log;
                    }
                }
            }
        }
        //add the data to the database
        $Item = new Item();
        $Item->setItemName($itemName);
        $Item->setItemDescription($itemDescription);
        $Item->setItemPrice($itemPrice);
        $Item->setBrand($brand);
        $Item->setModel($model);
        $Item->setOwnersID($ownerID);
        $Item->setImages($fileName);
        $Item->setCapacity($capacity);
        $Item->save(); //saves the data
        Util::CreateDialog('Saved', $Item->getItemName() . ' has been saved and waiting for admins approval' );
    }
}
//EOF