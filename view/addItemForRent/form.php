
<?php
if ($User->getConfirm() == 0) {
    Util::CreateDialog('Account Confirmation', 'Your account is not yet approved by the admin. Please ask for assistance from the admins of the website.');
} else {
    ?>
    <form id="addNewItemForm" method="post" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
        <input type="hidden" name="action" value="addItem">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12" id="imageHolder">
                        <img src="upload/defaults/imagePreview.jpg">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <span>&nbsp;</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 image-list" id="image-list">
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <h3>New Item</h3>
                    </div>
                    <div class="col-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="itemName">Item Name : </label>
                                <input type="text" class="form-control" id="itemName" name="itemName" placeholder="Item Name" required>
                            </div>
                            <div class="form-group">
                                <label for="itemDescription">Item Description : </label>
                                <textarea type="text"  name="itemDescription" class="form-control" id="itemDescription"
                                          placeholder="Item Description" rows="8 " style="resize: none;"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="brand">Brand : </label>
                                <select type="text" class="form-control" id="brand" name="brand">
                                    <option value="" disabled selected>Select A Brand</option>
                                    <?php
                                    $Brands = Brand::LoadArray();
                                    if (!empty($Brands)) {
                                        $html = null;
                                        foreach ($Brands as $Brand) {
                                            $html .= '<option value="' . $Brand->getBrandID() . '">' . $Brand->getBrandName() . '</option>';
                                        }
                                        print($html);
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="itemPrice">Price : </label>
                                <div class="row">
                                    <div class="col-md-5">
                                        <input type="number" class="form-control col-6" id="itemPriceFrom"
                                               name="itemPriceFrom" placeholder="Item Price From"
                                               required>
                                    </div>
                                    <div class="col-md-2">to</div>
                                    <div class="col-md-5">
                                        <input type="number" class="form-control col-6" id="itemPriceTo"
                                               name="itemPriceTo" placeholder="Item Price To"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="itemModel">Model : </label>
                                <input type="text" class="form-control" id="itemModel" name="itemModel" placeholder="Model">
                            </div>
                            <div class="form-group">
                                <label for="itemModel">Capacity : </label>
                                <input type="number" class="form-control" id="itemCapacity" name="capacity" placeholder="Capacity">
                            </div>
                            <div class="form-group">
                                <label for="images">Image Upload : </label>
                                <input type="file" name="image[]" id="image" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple />
                                <label for="image">
                                    <svg xmlns="http://www.w3.org/2000/svg"  width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Add Photos</span></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4 right float-right">
                                <button type="button" class="btn btn-danger float-right"  onclick="this.form.reset();">Reset</button>
                                <button type="button" class="btn btn-primary" onclick="window.history.back();">Cancel</button>
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>

    <?php
}