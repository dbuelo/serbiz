<?php
ini_set('upload_max_filesize', '20M');
$task = Util::getParam('task');
if (!empty($task)) {
    switch ($task) {
        case 'saveEdit';
            saveEdit();
            break;
    }
}

// ==============
// Functions for the task
function saveEdit()
{

    $User = User::GetCurrentUser();
    $email = Util::getParam('email');
    $fname = Util::getParam('fname');
    $address = Util::getParam('address');
    $contactNo = Util::getParam('contactNo');
    $path = 'upload/';
// This is to upload a profile picture
    if (isset($_FILES['profile'])) {
        $file = $_FILES['profile'];
        $ImageUpload = new Upload($file);
        $ImageUpload->file_new_name_body = "serbiz_";
        if ($ImageUpload->uploaded) {
            $ImageUpload->Process($path);
            if ($ImageUpload->processed) {
//upload success
                $fileName = $ImageUpload->file_dst_name;
            } else {
                echo 'error : ' . $ImageUpload->log;
            }
        }

        if (!empty($fname) && !empty($email)) {
            $data = [
                'user_fname' => $fname,
                'user_email' => $email,
                'address' => $address,
                'contact_no' => $contactNo
            ];
            if (!empty($fileName)) {
                $data['profile_img'] = $fileName;
            }
            $where = ['user_id' => $User->getUserID()];
            DBcon::update(User::TABLE_NAME, $data, $where);
            $_SESSION['User'] = serialize(User::Load($User->getUserID()));
        }
    }
}
