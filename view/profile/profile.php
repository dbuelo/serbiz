<?php
$User = User::GetCurrentUser();
$edit = Util::getParam('edit');

if (empty($edit)) {
    ?>
    <div class="col-md-12 text-right">
        <a href="profile.php?edit=true" class="btn btn-primary" style="color:white">Edit</a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <strong class="text-center"><h3>Your Profile</h3></strong>
            <div class="col-md-6 profile-image">
                <?php
                $image = $User->getProfileImage();
                if (!empty($image)) {
                    $html = '<img src="upload/' . $image . '">';
                } else {
                    $html = '<img src="upload/defaults/imagePreview.jpg">';
                }
                echo $html;
                ?>
            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="col-md-5">Name :</div>
                    <div class="col-md-7"><?= $User->getUserFname() ?></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5">Email :</div>
                    <div class="col-md-7"><?= $User->getUserEmail() ?></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5">Address :</div>
                    <div class="col-md-7"><?= $User->getAddress() ?></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5">Contact No. :</div>
                    <div class="col-md-7"><?= $User->getContactNo() ?></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5">Account Type :</div>
                    <div class="col-md-7"><?= $User->getRoleName() ?></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5">Account Status :</div>
                    <div class="col-md-7"><?= $User->getConfirm() ? "Confirmed" : "Pending" ?></div>
                </div>
            </div>
        </div>
    </div>
    <?php
} else {
    ?>
    <form method="post" action="<?= $_SERVER['PHP_SELF'] ?>" enctype="multipart/form-data">
        <div class="row">
            <input type="hidden" name="task" value="saveEdit">
            <div class="col-md-12">
                <strong class="text-center"><h3>Your Profile</h3></strong>
                <div class="col-md-6 profile-image">
                    <span id="imageHolder">
                        <?php
                        $image = $User->getProfileImage();
                        if (!empty($image)) {
                            $html = '<img src="upload/' . $image . '">';
                        } else {
                            $html = '<img src="upload/defaults/imagePreview.jpg">';
                        }
                        echo $html;
                        ?>
                    </span>
                    <input id="image" type="file" class="form-control" name="profile">
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="col-md-5">Name :</div>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="fname" value="<?= $User->getUserFname() ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-5">Email :</div>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="email" value="<?= $User->getUserEmail() ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-5">Address :</div>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="address"
                                   value="<?= $User->getAddress() ?>"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-5">Contact No. :</div>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="contactNo"
                                   value="<?= $User->getContactNo() ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-5">Account Type :</div>
                        <div class="col-md-7"><?= $User->getRoleName() ?></div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-5">Account Status :</div>
                        <div class="col-md-7"><?= $User->getConfirm() ? "Confirmed" : "Pending" ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-right">
            <button class="btn btn-success">Save<button>
        </div>
    </form>
    <?php

}