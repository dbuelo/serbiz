<?php
// handling the login
$username = Util::getParam('username');
if (isset($username) && !empty($username)) {
    $password = Util::getParam('pass');
    if (empty($username) || empty($password)) {
        $message = 'Error empty email or password';
    } else {
        $username = DBcon::clean($username);
        $password = md5(DBcon::clean($password));
        $sql = "SELECT
                    user_id
                FROM
                    " . User::TABLE_NAME . "
                WHERE
                    user_email ='{$username}'
                AND
                    user_pass ='{$password}'
                AND
                    remove = 0";
        $result = DBcon::execute($sql);
        $data = DBcon::fetch_assoc($result);
        if (empty($data)) {
            $message = 'Error wrong email or password';
        } else {
            $User = User::Load($data['user_id']);
            Util::debug($User);
            $_SESSION['User'] = serialize($User);
            if ($User->getRole() == User::ADMIN) {
                Util::redirect('admin/index.php');
            } else {

                if($User->isConfirmed()){
                    $_SESSION['message'] = ['title' => 'Success', 'message' => 'Successfully Log in'];
                    Util::redirect('index.php');
                } else {
                    unset($_SESSION['User']);
                    $_SESSION['message'] = ['title' => 'Error', 'message' => 'Account not yet confirmed by the admins'];
                    Util::redirect('login.php');
                }
            }
        }
    }
}