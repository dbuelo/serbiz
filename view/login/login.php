<div class="formContainer">
    <div class="row top-buffer">
        &nbsp;
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-form">
                    <?php
                    if (isset($message) && !empty($message)) {
                        if(is_array($message)){
                            $message = $message['message'];
                        }
                        echo '<p class="text-center alert-message">' . $message . "</p>";
                    }
                    ?>
                    <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
                        <h2 class="text-center">Log in</h2>
                        <div class="form-group">
                            <input type="email" name="username" class="form-control custom-input" placeholder="Email"
                                   required="required" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <input type="password" name="pass" class="form-control custom-input" placeholder="Password"
                                   required="required" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit">Login</button>
                        </div>
                        <p class="text-center"><a href="register.php" class="text-info">Create an account</a></p>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>