<?php
// handle searching
$search = Util::getParam('searchTerm');
if (!empty($search)) {
    $sql = "SELECT
                item_id
            FROM
                items
            WHERE
                (item_name LIKE '%{$search}%' OR item_description LIKE '%{$search}%')
            AND
                archive = 0
            AND
                status = " . Item::STATUS_APPROVED;
} else {
    $sql = 'SELECT
            item_id
        FROM
            items
        WHERE
            archive = 0
        AND
            status = ' . Item::STATUS_APPROVED;
}
$object = DBcon::execute($sql);
$results = DBcon::fetch_all_assoc($object);
$temp = [];
if (!empty($results)) {
    foreach ($results as $result) {
        $temp[] = $result['item_id'];
    }
    $itemList = Item::LoadArray($temp);
}
