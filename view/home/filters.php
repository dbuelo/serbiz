

<div class="row text-center center">
    <div class="col-md-10 center text-center">
        <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
            <div class="input-group mb-12">
                <input class="form-control" type="text" name="searchTerm" placeholder="Search">
                <div class="input-group-btn">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
