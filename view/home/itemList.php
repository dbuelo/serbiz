<?php

if (!empty($itemList)) {
    shuffle($itemList);
    $html = '<div class="row">';
    foreach ($itemList as $Item) {
        $images = $Item->getImages();
        $imageToDisplay = !empty($images) ? $images[0] : 'defaults/imagePreview.jpg';
        if (!file_exists('upload/' . $imageToDisplay)) {
            $imageToDisplay = 'defaults/imagePreview.jpg';
        }
        $html .= '
            <div class="col-md-3 ">
                <div class="item-box">
                    <div class="image-container">
                        <img src="upload/' . $imageToDisplay . '">
                    </div>
                    <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 details">
                            <div class="col-md-12 col-sm-12 col-s-12 pill">
                                <a href="itemDetails.php?id=' . $Item->getItemID() . '" data-toggle="tooltip" data-placement="left" title="' . $Item->getItemName() . '">' . $Item->getItemName() . ' </a>
                            </div>
                            <div class="col-md-12 col-sm-12 col-s-12 pill">
                                ' . $Item->getItemPriceRange() . '
                            </div>
                            <div class="col-md-12 col-sm-12 col-s-12 pill">
                                ' . $Item->getOwnersDetails()->getAddress() . '
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>';
    }
    $html .= '</div>';
    print $html;
} else {
    ?>
    <h2 class="text-center">No Items to show</h2>
    <?php
}