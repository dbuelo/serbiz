<div class="modal" id="writeComment">
    <div class="modalContainer small-modal">
        <div class="header">
            Review '<?= $Item->getItemName() ?>'
            <div class="close">x</div>
        </div>
        <div class="content small-modal">
            <form action="itemDetails.php?id=<?= $Item->getItemID(); ?>" id="commentForm" method="post">
                <input type="hidden" name="">
                <input type="hidden" name="itemID" value="<?= $Item->getItemID(); ?>">
                <input type="hidden" name="action" value="saveComment">
                <div class="row">
                    <div class="col-md-12 details">
                        <strong>Comment </strong>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 details">
                        <textarea name="comment" row="20"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 details">
                        <select name="rate" class="col-md-12">
                            <option value="0" disabled="" selected>Rate this Item</option>
                            <option value="1">1 star</option>
                            <option value="2">2 stars</option>
                            <option value="3">3 stars</option>
                            <option value="4">4 stars</option>
                            <option value="5">5 stars</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 details text-right">
                        <button type="button" class="btn btn-default" id="cancel">Cancel</button>
                        <button class="btn btn-primary">Post Comment</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Message Query Form -->
<div class="modal" id="writeQuery">
    <div class="modalContainer small-modal">
        <div class="header">
            Query for '<?= $Item->getItemName() ?>'
            <div class="close">x</div>
        </div>
        <div class="content small-modal">
            <form action="itemDetails.php?id=<?= $Item->getItemID(); ?>" id="commentForm" method="post">
                <input type="hidden" name="">
                <input type="hidden" name="itemID" value="<?= $Item->getItemID(); ?>">
                <input type="hidden" name="action" value="sendQuery">
                <div class="row">
                    <div class="col-md-12 details">
                        <strong>Message </strong>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 details">
                        <textarea name="message" row="20"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 details text-right">
                        <button type="button" class="btn btn-default" id="cancel">Cancel</button>
                        <button class="btn btn-primary">Send Query</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Reservation form -->
<div class="modal" id="reservationModal">
    <div class="modalContainer small-modal">
        <div class="header">
            Reserve '<?= $Item->getItemName() ?>'
            <div class="close">x</div>
        </div>
        <div class="content small-modal">
            <form action="itemDetails.php?id=<?= $Item->getItemID(); ?>" id="commentForm" method="post">
                <input type="hidden" name="itemID" value="<?= $Item->getItemID(); ?>">
                <input type="hidden" name="action" value="saveReservation">
                <div class="form-group">
                    <label for="pickupDate">Pick up Date</label>
                    <input type="date" class="form-control" id="pickupDate" name="pickupDate" required>
                </div>
                <div class="form-group">
                    <label for="pickOffDate">Pick off Date</label>
                    <input type="date" class="form-control" id="pickOffDate" name="pickOffDate" required>
                </div>
                <div class="form-group">
                    <label for="pickUpLocation">Pick up location</label>
                    <input type="text" class="form-control" id="pickUpLocation" name="pickUpLocation" required>
                </div>
                <div class="form-group">
                    <label for="passengerNo">Passenger No.</label>
                    <input type="number" min="1" class="form-control" id="passengerNo" name="passengerNo" required>
                </div>
                <div class="form-group">
                    <label for="notes">Notes</label>
                    <textarea class="form-control" id="notes" name="notes"></textarea>
                </div>
                <div class="row">
                    <div class="col-md-12 details text-right">
                        <button type="button" class="btn btn-default" id="cancel">Cancel</button>
                        <button class="btn btn-primary">Reserve</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>