<?php
$Item = Item::Load($itemID);
$images = $Item->getImages();
$imageToDisplay = !empty($images) ? $images[0] : 'defaults/imagePreview.jpg';
if (!file_exists('upload/' . $imageToDisplay)) {
    $imageToDisplay = 'defaults/imagePreview.jpg';
}
$User = User::GetCurrentUser()

?>
<h3><?= $Item->getItemName() ?></h3>
<div class='detailsContainers'>
    <div class="row">
        <div class="col-md-4">
            <div class="imageHolder">
                <img src="upload/<?= $imageToDisplay ?>">
            </div>
        </div>
        <div class="col-md-4">
            <div class="detailsHolder">
                <div class="col-md-12">
                    <h2><?= $Item->getItemName() ?></h2>
                </div>
                <div class="col-md-12">
                    <p><?= $Item->getItemDescription() ?></p>
                </div>
                <hr class="border-dark">
                <div class="col-md-12">
                    <h3 class="text-secondary">
                        <?= $Item->getItemPriceRange() ?>
                    </h3>
                </div>
                <div class="col-md-12">
                    <strong>Rating : </strong>
                    <?php
                    $rate = Reviews::GetAverageRate($itemID);
                    for ($i = 0; 5 > $i; $i++) {
                        if ($i < $rate) {
                            echo '<i class="text-warning fas fa-star"></i>';
                        } else {
                            echo '<i class="text-warning far fa-star"></i>';
                        }
                    }
                    ?>
                </div>
                <hr class="border-dark">
                <?php if ($User !== false && $User->getUserID() !== $Item->getOwnersID() ) { ?>
                    <div class="col-md-12">
                        <button class="btn btn-primary clickable" data-target="modal" data-id="reservationModal" >Reserve</button>
                        <button class="btn btn-info" id="writeQueryButton">Send Query</button>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-4">
            <ul class="nav nav-left-sidebar nav-container">
                <li><i class="fa fa-car"></i> <?= $Item->getBrandName() ?>  <?= !empty($Item->getModel()) ? "(" . $Item->getModel() . ")" : '' ?></li>

                <li><i class="fa fa-calendar"></i> <?php
                    $availability = Reservation::IsAvailable($Item->getItemID());
                    $toDisplay = $availability ? 'Available' : 'Not Available On This Day';
                    echo $toDisplay;
                    ?></li>
                <li><i class="fa fa-car"></i> <?= $Item->getCapacity(); ?> seats</li>
                <li><i class="fa fa-map-marked-alt"></i> <?= $Item->getOwnersDetails()->getAddress(); ?></li>
                <li><i class="fa fa-address-book"></i> <?= $Item->getOwnerName(); ?></li>
            </ul>
        </div>
    </div>

</div>
<br>
<?php
if ($User !== false && $User->getUserID() !== $Item->getOwnersID() ) {
    
    ?>
    <span class="right"><button class="btn  btn-info" id="writeCommentButton">Write a Comment</button></span>
    <?php
}
?>
<h2>Reviews and Comments</h2>
<div class='detailsContainers'>
    <?php
    $reviews = Reviews::LoadReviewsByItem($Item->getItemID());
    if (!empty($reviews)) {
        foreach ($reviews as $Review) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <?= Util::date($Review->getDate()) ?>
                    <div class="col-md-12 bg-white commentContainer">
                        <?= $Review->getComment() ?>
                    </div>
                </div>
            </div>
            <?php
        }
    } else {
        ?>
        <div class="row">
            <div class="col-md-12 text-center">No Reviews</div>
        </div>
        <?php
    }
    ?>
</div>