<?php
$task = Util::getParam('action');
switch ($task) {
    case 'saveComment':
        $itemID = Util::getParam('itemID');
        $comment = Util::getParam('comment');
        $rate = Util::getParam('rate');
        if (!empty($comment)) {
            $Review = Reviews::Create();
            $Review->setComment($comment);
            $Review->setItemID($itemID);
            $Review->setRate($rate);
            $Review->save();
        } else {
            Util::CreateDialog('Error', 'Comment must not be empty');
        }
        break;
    case 'sendQuery':
        $itemID = Util::getParam('itemID');
        $messageEntry = Util::getParam('message');
        if (!empty($messageEntry)) {
            $Chat = new Chat();
            $User = User::GetCurrentUser();
            $Chat->setSenderID($User->getUserID());
            $Item = Item::Load($itemID);
            $to = $Item->getOwnersID();
            $Chat->setReceiverID($to);
            $message = 'Title : Query for \"' . $Item->getItemName() . '\"';
            $message .= '\n\n';
            $message .= $messageEntry;
            $Chat->setMessage($message);
            $Chat->submit();
        } else {
            Util::CreateDialog('Error', 'Message must not be empty');
        }
        break;
    case 'saveReservation':
        $itemID = Util::getParam('itemID');
        $pickupDate = Util::getParam('pickupDate');
        $pickOffDate = Util::getParam('pickOffDate');
        $pickUpLocation = Util::getParam('pickUpLocation');
        $passengerNo = Util::getParam('passengerNo');
        $notes = Util::getParam('notes');
        $reservationData = [
            'itemID' => $itemID,
            'pickupDate' => $pickupDate,
            'pickOffDate' => $pickOffDate,
            'pickUpLocation' => $pickUpLocation,
            'passengerNo' => $passengerNo,
            'notes' => $notes
        ];
        if (!empty($itemID)) {
            $reserve = Reservation::Reserve($reservationData);
        }
        break;
}
