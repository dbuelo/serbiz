<?php
require_once 'init.php';
$Outline->addCSS('css/profile.css');
$Outline->addJS('js/inputfileJs.js');
$Outline->header('My Profile');
require_once 'view/profile/handler.php';
require_once 'view/profile/profile.php';
$Outline->footer();