<?php
require_once('init.php');
$Layout = new Layout();
$Layout->addCSS($css);
$Layout->addCSS('css/login.css');
$Layout->addJS($js, false);
$Layout->addJS('js/validation.js');
$Layout->header('Registration', false);
require_once('view/register/handler.php');
require_once('view/register/register.php');
$Layout->footer();