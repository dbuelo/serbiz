<?php
include_once 'init.php';
$Layout = new Layout();
$Layout->addCSS($css);
$Layout->addCSS('css/login.css');
$Layout->addJS($js,false);
$Layout->addJS(['js/modalHandler.js','js/general.js']);
$Layout->header('Login',false);
require_once('view/login/handler.php');
require_once('view/login/login.php');
$Layout->footer();
//EOF