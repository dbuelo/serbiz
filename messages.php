<?php
/*
 * Show messages
 */
include_once 'init.php';
$userId = $User->getUserID();
$Outline->addCSS('css/message.css');
$Outline->addJS('js/message.js');
$Outline->header('Messages');
$Chats = Chat::getChatsForMe($userId);
$ChaterIDs = array_keys($Chats);
?>
    <div class="row messageHolder">
        <div class="tab col-md-3">
            <button type="button" class="btn btn-primary btn-lg modalTriggerShow" data-toggle="modal"
                    data-target="#gridSystemModal">
                <i class="fas fa-plus"></i> New Message
            </button>

            <?php
            $html = null;
            $firstElement = reset($ChaterIDs);
            foreach ($ChaterIDs as $chatmate) {
                if($chatmate == Chat::SENDER_SYSTEM){
                    $nametoshow = "<b>System Bot</b>";
                } else {
                    $UserName = User::getName($chatmate);
                    $nametoshow = !empty($UserName) ? $UserName : User::getEmailAddress($chatmate);
                }
                if ($chatmate == $firstElement) {
                    $html .= '<button class="tablinks active" data-sender=\'' . $chatmate . '\' onclick="openCity(event, \'' . $chatmate . '\')">' . $nametoshow . '</button>';
                } else {
                    $html .= '<button class="tablinks" onclick="openCity(event, \'' . $chatmate . '\')">' . $nametoshow . '</button>';
                }
            }
            echo $html;
            ?>

        </div>
        <?php
        foreach ($Chats as $key => $chat) {
            ?>
            <div id="<?= $key ?>" class="tabcontent col-md-9 <?= $firstElement == $key ? "active" : null ?>">
                <?php
                if($key == Chat::SENDER_SYSTEM){
                    $nametoshow = "<b>System Bot</b>";
                } else {
                    $UserName = User::getName($key);
                    $nametoshow = !empty($UserName) ? $UserName : User::getEmailAddress($chatmate);
                }
                ?>
                <h3><?= $nametoshow ?></h3>
                <div class="messageBlock align-bottom">
                    <?php
                    $html = null;
                    ksort($chat);
                    foreach ($chat as $message) {
                        $timeSent = date("m/d/Y h:ia", strtotime($message->getTimesent()));
                        if ($message->getSenderID() == $User->getUserID()) {
                            $html .= "<div class='row'><div class='myMessage'>";
                            if( !empty($message->getAttachment())){
                                $File = File::Load($message->getAttachment());
                                $image = ImageHandler::ReconstructImage($File->getBytes(),$File->getMime());

                                $html .= "<img class='attachment' src='" . $image . "'>";
                            }
                            $html .= $message->getMessage();
                            $html .= "</div></div>";
                            $html .= "<div class='row'><div class='myTimesent'>{$timeSent}</div></div>";
                        } else {
                            $html .= " <div class='row'><div class='userMessage'>";

                            if( !empty($message->getAttachment())){
                                $File = File::Load($message->getAttachment());
                                $image = ImageHandler::ReconstructImage($File->getBytes(),$File->getMime());

                                $html .= "<img src='" . $image . "'>";
                            }
                            $html .="{$message->getMessage()}</div></div>";
                            $html .= "<div class='row'><div class='timesent'>{$timeSent}</div></div>";
                        }
                    }
                    echo $html;
                    ?>
                </div>
                <div class="messageForm">
                    <form action="processMessage.php" method="post" enctype="multipart/form-data">
                    <span class="p-5">
                    <label for="file-upload" class="custom-file-upload">
                        <input id="file-upload" type="file" name="attachment"/>
                        <i class="fas fa-image" style="font-size: 30px"></i>
                    </label>
                    <input type="hidden" name="to" value="<?= $key ?>">
                    </span>
                        <input class="inputMessage" type="text" name="message">
                        <button class="btn btn-secondary">Send</button>
                    </form>
                </div>
            </div>
            <?php
        }
        ?>
    </div>

    <!-- Mobile Responsiveness -->
    <div class="mobile ">
        <div class="messagesList">
            <button type="button" class="btn btn-primary btn-lg modalTriggerShow" data-toggle="modal"
                    data-target="#gridSystemModal">
                <i class="fas fa-plus"></i> New Message
            </button>

            <?php
            $html = null;
            foreach ($ChaterIDs as $chatmate) {
                $UserName = User::getName($key);
                $nametoshow = !empty($UserName) ? $UserName : User::getEmailAddress($chatmate);
                $html .= '<button class="tablinks mobileTrigger" data-sender="' . $chatmate . '">' .$nametoshow . '</button>';
            }
            echo $html;
            ?>
        </div>
        <?php
        foreach ($Chats as $key => $chat) {
            ?>
            <div id="mobileContent<?= $key ?>" class=" tabcontent col-md-9  mobileContent">
                <button class="mobileTriggerBack">Back</button>
                <?php
                $user = User::getName($key);
                ?>
                <h3><?= $user ?></h3>
                <div class="messageBlock align-bottom">
                    <?php
                    $html = null;
                    ksort($chat);
                    foreach ($chat as $message) {
                        $timeSent = date("m/d/Y h:ia", strtotime($message->getTimesent()));
                        if ($message->getSenderID() == $User) {
                            $html .= " <div class='row'><div class='myMessage'>{$message->getMessage()}</div></div>";
                            $html .= "<div class='row'><div class='myTimesent'>{$timeSent}</div></div>";
                        } else {
                            $html .= " <div class='row'><div class='userMessage'>{$message->getMessage()}</div></div>";
                            $html .= "<div class='row'><div class='timesent'>{$timeSent}</div></div>";
                        }
                    }
                    echo $html;
                    ?>
                </div>
                <div class="messageForm">
                    <form action="processMessage.php" method="post" enctype="multipart/form-data">
                        <input type="file" name="attach[]">
                        <input type="hidden" name="to" value="<?= $key ?>">
                        <input class="inputMessage" type="text" name="message">
                        <button class="btn btn-secondary">Send</button>
                    </form>
                </div>
            </div>
            <?php
        }
        ?>
    </div>

<?php
require 'view/messageModal.php';
?>
    <script>
        $(document).ready(function () {
            $('.mobileTrigger').click(function () {
                var key = $(this).data('key');

                $('#mobileContent' + key).fadeIn();
                $('.messagesList').fadeOut();
            });

            $('.mobileTriggerBack').click(function () {
                $('.mobileContent').fadeOut();
                $('.messagesList').fadeIn();
            });

            $('.tablinks').click(function () {
                var chatSender = $(this).data('sender');
                $.ajax({
                    url: 'processMessage.php',
                    data: {
                        'chatSender': chatSender,
                        'task': 'unread'
                    },
                    success: function (response) {
                        let badge = $('.message-badge').html();
                        if (response !== 0) {
                            let count = badge - response;
                            $('.message-badge').html(count);
                        }
                    }
                });
            });
        });

        function openCity(evt, cityName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the link that opened the tab
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

    </script>
<?php
$Outline->footer();
