<?php
include_once 'init.php';
$Outline->addCSS('css/itemlist.css');
$Outline->header('Home');
require_once('view/home/handler.php');
require_once('view/home/filters.php');
require_once('view/home/itemList.php');
$Outline->footer();
