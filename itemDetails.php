<?php
require_once 'init.php';
$css = [
    'css/details.css',
    'css/modals.css'
];

$itemID = Util::getParam('id');
$Outline->addCSS($css);
$Outline->header('Item Details');
if (!empty($itemID)) {
    require_once 'view/itemDetails/handler.php';
    require_once 'view/itemDetails/details.php';
    require_once 'view/itemDetails/modals.php';
} else {
    print 'Item not found';
}
$Outline->footer();
