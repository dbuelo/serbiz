<?php
require '../../init.php';
const TABLE_ITEM = 'items';
$itemID = $_GET['itemID'];
$action = $_GET['action'];
$data = [
    'status' => 1
];
$where = [
    'item_id' => $itemID
];
switch ($action) {
    case 0: $result = Dbcon::update(TABLE_ITEM, $data, $where);
        break;
    case 1: $result = Dbcon::delete(TABLE_ITEM, $where);
        break;
}
Util::redirect('../item_view.php');
//EOF