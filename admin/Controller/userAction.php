<?php
require '../../init.php';
const TABLE_USER = 'tbl_users';
$action = (int) $_GET['action'];
$id = $_GET['userID'];
$db = new Dbcon();
switch ($action) {
    case 1 :
        $data = [
            'confirm' => 1
        ];
        $where = [
            'user_id' => $id
        ];
        $result = $db::update(TABLE_USER, $data, $where);
        break;
    case 2:
        $where = [
            'user_id' => $id
        ];
        $db::delete(TABLE_USER, $where);
        break;
}
Util::redirect('../users_view.php');
//EOF