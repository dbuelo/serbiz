<?php
require_once 'header.php';
require_once '../init.php';
$id = Util::getParam('id');
if (!empty($id)) {
    $userDetails = User::Load($id);
}
?>
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <?php
        if (isset($userDetails)) {
            ?>
            <table>
                <tr>
                    <td rowspan="6" class="p-3">
                        <img src='../upload/defaults/avatar.png' width="200px">
                    </td>
                    <td class="p-2">Name :</td>
                    <td class="p-2"><?= $userDetails->getUserFname() ?></td>
                </tr>
                <tr>
                    <td class="p-2">Email :</td>
                    <td class="p-2"><?= $userDetails->getUserEmail() ?></td>
                </tr>
                <tr>
                    <td class="p-2">Contact No :</td>
                    <td class="p-2"><?= $userDetails->getContactNo() ?></td>
                </tr>
                <tr>
                    <td class="p-2">Address :</td>
                    <td class="p-2"><?= $userDetails->getAddress() ?></td>
                </tr>
                <tr>
                    <td class="p-2">Account Type  :</td>
                    <td class="p-2"><?= $userDetails->getRoleName() ?></td>
                </tr>
                <tr>
                    <td class="p-2">Account Status :</td>
                    <td class="p-2"><?= $userDetails->getConfirm() ? "Confirmed" : "Pending" ?></td>
                </tr>
            </table>
            <h3>Attachment</h3>
            <?php
            $attachments = $userDetails->getAttachment();
            if (!empty($attachments)) {
                echo '<img src="../upload/' . $attachments . '" width="50%">';
            } else {
                echo 'No Attachments found';
            }
        } else {
            echo 'no user details';
        }
        ?>
    </div>
</div>

<script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<script src="assets/vendor/bootstrap_old/js/bootstrap.bundle.js"></script>
<script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
<script src="assets/vendor/multi-select/js/jquery.multi-select.js"></script>
<script src="assets/libs/js/main-js.js"></script>
<script src="assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="assets/vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/vendor/datatables/js/dataTables.buttons.min.js"></script>
<script src="assets/vendor/datatables/js/buttons.bootstrap4.min.js"></script>
<script src="assets/vendor/datatables/js/data-table.js"></script>
<script src="../../../../../cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="../../../../../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="../../../../../cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="../../../../../cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="../../../../../cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="../../../../../cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script src="../../../../../cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js"></script>
<script src="../../../../../cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
<script src="../../../../../cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
</body>
</html>