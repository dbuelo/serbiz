<?php
require_once '../init.php';

$task = Util::getParam('task');

require_once 'view/brands/handler.php';
require_once 'header.php';
?><div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-md-12">
                <h3 class="d-inline">Brands</h3>
                <?php
                if (empty($task) || $task !== 'addBrands') {
                    ?>
                    <a href="brands.php?task=addBrands" class="btn btn-facebook d-inline float-right">Add New Brand</a>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
        if (!empty($task) && $task === 'addBrands') {
            require_once 'view/brands/form.php';
        } else {
            require_once 'view/brands/brands.php';
        }
        ?>
    </div>
</div>
<?php
require_once 'footer.php';
