<?php
require_once '../init.php';
require 'header.php';
$deletID = Util::getParam('id');
if (!empty($deletID)) {
    $sql = 'UPDATE tbl_user SET remove = 1 WHERE user_id= ' . $deletID;
    DBcon::execute($sql);
}

$task = Util::getParam('task');
if($task === 'delete'){
    $id = Util::getParam('id');
    if(!empty($id) && $id !== false){
        Dbcon::update(Dbcon::TABLE_USERS,['remove' => date('Y-m-d')],['user_id' => $id]);
    }
}


$sql = 'SELECT
            user_id,
            user_fname,
            user_email,
            profile_img
        FROM
            tbl_users
        WHERE
            remove = 0
        AND
            confirm = 1
        AND
            role <> 5';
        $results = DBCon::fetch_sql_assoc($sql, ['user_id']);
?>

<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <section>
            <div class="row">
                <div class="col-md-12"><h3>Delete Users</h3></div>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="col-md-12">
                    <form method="POST" id="search">
                        <div class="form-group">
                            <input type="text"  class="form-control" placeholder="Search..">
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <section>
            <table class="table">
                <thead>
                    <tr>
                        <th>User ID</th>
                        <th>User Email</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($results)) {
                        foreach ($results as $user) {
                            ?>

                            <tr>
                                <td><?= $user['user_id'] ?></td>
                                <td><?= $user['user_email'] ?></td>
                                <td><?= $user['user_fname'] ?></td>
                                <td>
                                   <!-- <button class="btn btn-success btn-modal" data-toggle="modal" data-target="#userDetailModal<?= $user['user_id'] ?>">View Details</button> --> <a class="btn btn-danger" href="deleteUsers.php?id=<?= $user['user_id'] ?>&task=delete">Delete</a></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </section>
    </div>
</div>
<?php
if (!empty($results)) {
    foreach ($results as $user) {
        ?>
        <div class="modal fade" id="userDetailModal<?= $user['user_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close btn-modal" data-target="#userDetailModal<?= $user['user_id'] ?>" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                       Test user
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>

<script>
    $(document).ready(function () {

        $('.btn-modal').on('click', function () {
            let el = $(this);
            let target = el.data('target');
            $(target).toggleClass('show');
            $(target).slideToggle('slow');
        })

    });
</script>
<?php
require_once 'footer.php';
