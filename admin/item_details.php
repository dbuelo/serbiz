<!doctype html>
<html lang="en">


    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/vendor/bootstrap_old/css/bootstrap.min.css">
        <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/libs/css/style.css">
        <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
        <title>Serbiz | Admin</title>
    </head>

    <?php
    include_once("header.php");
    ;
    require '../init.php';
    $itemID = Util::getParam('itemID');
    if (!empty($itemID)) {
        $Item = Item::Load($itemID);
    }
    ?>
    <!-- ============================================================== -->
    <!-- wrapper  -->
    <!-- ============================================================== -->
    <div class="dashboard-wrapper">
        <div class="influence-profile">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h3 class="mb-2"><?= $Item->getItemName() ?></h3>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit
                                amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Items</a></li>
                                        <li class="breadcrumb-item"><a href="item_view.php" class="breadcrumb-link">View All
                                                Items</a></li>
                                        <li class="breadcrumb-item active"
                                            aria-current="page"><?= $Item->getItemName() ?></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- profile -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <!-- ============================================================== -->
                        <!-- card profile -->
                        <!-- ============================================================== -->
                        <div class="card">
                            <div class="card-body">
                                <!-- ========= CAROUSEL =========== -->
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">

                                    </ol>
                                    <div class="carousel-inner">
                                        <?php
                                        $images = $Item->getImages();
                                        if (!empty($images)) {
                                            $html = '';
                                            foreach ($images as $image) {
                                                $html .= '<div class="carousel-item active">';
                                                $html .= '<img class="d-block" style="width:800px; height:375px; background-position: center center;background-repeat: no-repeat;" src="../upload/' . $image . '"></div>';
                                            }
                                            echo $html;
                                        }
                                        ?>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span> </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span> </a>
                                </div>
                                <!-- ============ END OF CAROUSEL -->

                                <div class="text-center">
                                    <h2 class="font-24 mb-0"><?= $Item->getItemName(); ?></h2>
                                    <p><?= $Item->getItemDescription(); ?></p>
                                </div>
                            </div>
                            <div class="card-body border-top">
                                <h3 class="font-16">Information</h3>

                                    <h1 class="margin-bottom"><?php echo $Item->getItemName(); ?></h1>

                                    <!-- Pruducts -->
                                    <div class="margin2x">
                                        <div class="s-12 m-8 l-2 xl-7 xl-3">

                                            <div class="line hide-m">
                                                <div id="header-carousel" class="owl-carousel owl-theme">

                                                </div>
                                            </div>

                                            <h5><strong><?php echo money_formater($Item->getItemPrice()); ?></strong></h5>

                                            <h4>Description:</h4>
                                            <p class="margin-bottom"><?php echo $Item->getItemDescription(); ?></p>
                                            <h4>Model:</h4>
                                            <p class="margin-bottom"><?php echo $Item->getModel(); ?></p>
                                            <h4>Brand:</h4>
                                            <p class="margin-bottom"><?php echo $Item->getBrandName(); ?></p>
                                            <h4>Date Added:</h4>
                                            <p class="margin-bottom"><?php echo $Item->getDateAdded(); ?></p>

                                            <?php
                                            ?>
                                            <div class="card">
                                                <div class="row">
                                                    <div class="col-md-2 col-lg-2 col-s-2 col-xl-2">
                                                        <?php
                                                        $action = [
                                                            'approve' => 0,
                                                            'decline' => 1
                                                        ];
                                                        if ($Item->getStatus() == 0) {
                                                            echo '<a href="Controller/itemAction.php?action=' . $action['approve'] . '&itemID=' . $Item->getItemID() . '"><button class="btn btn-primary"><i class="fa fa-check"></i> Approve</button></a>';
                                                        ?>
                                                    </div>
                                                    <div class="col-md-2 col-lg-2 col-s-2 col-xl-2">
                                                        <a href="Controller/itemAction.php?action=<?= $action['decline']; ?>&itemID=<?=
                                                        $Item->getItemID();
                                                    }
                                                    ?>">
                                                        <button class="btn btn-secondary"><i class="fa fa-gavel"></i>
                                                            Decline
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <!-- ============================================================== -->
                                    <!-- end card profile -->
                                    <!-- ============================================================== -->
                                </div>
                                <!-- ============================================================== -->
                                <!-- end profile -->
                                <!-- ============================================================== -->

                            </div>
                            <!-- ============================================================== -->
                            <!-- end content -->
                            <!-- ============================================================== -->

                            <!-- ============================================================== -->
                            <!-- end footer -->
                            <!-- ============================================================== -->
                        </div>
                        <!-- ============================================================== -->
                        <!-- end wrapper -->
                        <!-- ============================================================== -->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end main wrapper -->
                <!-- ============================================================== -->
                <!-- Optional JavaScript -->
                <!-- jquery 3.3.1  -->
                <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
                <!-- bootstap bundle js -->
                <script src="assets/vendor/bootstrap_old/js/bootstrap.bundle.js"></script>
                <!-- slimscroll js -->
                <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
                <!-- main js -->
                <script src="assets/libs/js/main-js.js"></script>
                <script src="https://maps.google.com/maps/api/js?key=AIzaSyC5TaSNk0MhM7hdBCVi6I_MvzsXR8ozOi0&callback=myMap"></script>
                </body>

                </html>
                <?php

                function money_formater($value)
                {
                    return 'Php ' . number_format($value, 2);
                }
                ?>