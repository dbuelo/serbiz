<?php
if (!empty($task)) {
    switch ($task) {
        case 'saveBrand':
            $brandName = Util::getParam('brandName');
            $id = Util::getParam('id');

            if (!empty($brandName)) {
                if (!empty($id)) {
                    DBcon::update(Brand::TABLE_NAME, ['brand_name' => $brandName], ['brand_id' => $id]);
                } else {
                    Dbcon::insert(Brand::TABLE_NAME, ['brand_name' => $brandName]);
                }
                Util::redirect('brands.php');
            } else {
                Util::CreateDialog('Error', 'Empty Brand name');
            }
            break;
        case 'delete':
            $id = Util::getParam('id');
            DBcon::update(Brand::TABLE_NAME, ['archive' => 1], ['brand_id' => $id]);
            Util::redirect('brands.php');
            break;
    }
}