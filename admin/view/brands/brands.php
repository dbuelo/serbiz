<?php
$Brands = Brand::LoadArray();
?>
<table>
    <tr>
        <th class="p-2"> Brand Name </th>
        <th class="p-2"> Action </th>
    </tr>
    <?php
    if (!empty($Brands)) {
        $html = '';
        foreach ($Brands as $Brand) {
            $html .= '<tr><td class="p-2">' . $Brand->getBrandName() . '</td>';
            $html .= '<td class="p-2"><a class="btn btn-success" href="brands.php?task=addBrands&id=' . $Brand->getBrandID() . '">Edit</a> <a class="btn btn-danger" href="brands.php?task=delete&id=' . $Brand->getBrandID() . '">Delete</a></td></tr>';
        }
        echo $html;
    } else {
        echo 'No Brands Found';
    }
    ?>

</table>
