<?php
require_once 'init.php';
$js = [
    'js/inputfileJs.js'
];
$Outline->addJS($js);
$Outline->addCSS('css/addItemForRent.css');
if (isset($_SESSION['User'])) {
    $Outline->header('Post an Item');
    $User = unserialize($_SESSION['User']);
    require_once 'view/addItemForRent/handler.php';
    require_once 'view/addItemForRent/form.php';
    $Outline->footer();
} else {
    Util::redirect('login');
}