<?php
// will process the messages
include_once 'init.php';
$User = unserialize($_SESSION['User']);
$sender = $User->getUserID();
$to = Util::getParam('to');
$message = Util::getParam('message');
$backLink = $_SERVER['HTTP_REFERER'];
$task = Util::getParam('task');


if (!empty($task)) {
    if ($task == 'unread') {
        $chatSender = Util::getParam('chatSender');
        echo Chat::Unread($chatSender);
    }
} else {
    if (!is_numeric($to)) {
        $id = User::FindEmail($to);
        if ($id) {
            $chat = new Chat();
            $chat->setReceiverID($id);
            $chat->setSenderID($sender);
            $chat->setMessage($message);
            $chat->submit();
            Util::redirect($backLink);
        } else {
            $error = ['error' => 'failed', 'message' => 'Failed to send. User doesn\'t exist '];
            $_SESSION['error'] = $error;
            Util::redirect($backLink);
        }
    } else {
        $chat = new Chat();
        $chat->setReceiverID($to);
        $chat->setSenderID($sender);
        $chat->setMessage($message);

        //initialize File

        if( isset($_FILES['attachment'])){
            $file = $_FILES['attachment'];
            $bytes = ImageHandler::GetImageRaw($file['tmp_name'],$file['type']);
            $File = new File();
            $File->setBytes($bytes);
            $File->setMime($file['type']);
            $fileID = $File->save();
            $chat->setAttachment($fileID);
        }
        $chat->submit();
        Util::redirect($backLink);
    }
}
