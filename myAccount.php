<?php
include_once 'init.php';
// if the user is not login will be redirected to the login page
if (!isset($_SESSION['User'])) {
    Util::redirect('login/');
}
$Outline->header('My Account');
require 'view/myAccount/handler.php';
require 'view/myAccount/myAccount.php';

$Outline->addJS('common/js/myAccount.js');
$Outline->footer();
//EOF