<?php
include_once 'init.php';
$userid = $User->getUserID();

$css = [
    'css/myProp.css',
    'wowSlider/engine1/style.css'
];

$jsFooter = [
    'wowSlider/engine1/wowslider.js',
    'wowSlider/engine1/script.js',
    'js/itemDetailsModal.js'
];

$Outline->addJS($jsFooter);
$Outline->addCSS($css);
$Outline->header('My Properties');
//loads the properties of the current user
$edit = Util::getParam('task');
$id = Util::getParam('id');
require_once 'view/manageProp/handler.php';
if (!empty($edit) && !empty($id)) {
    require_once 'view/manageProp/editDetails.php';
} else {
    require_once 'view/manageProp/list.php';
    require_once 'view/manageProp/modal.php';
}
$Outline->footer();
