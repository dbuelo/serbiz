$(document).ready(function () {
    $('.btn').click(function () {
        let id = $(this).data('id');
        $('#item_' + id).addClass('show');
    });
    $('.close').click(function(){
        $(this).parents().parents().removeClass('show');
    });
});