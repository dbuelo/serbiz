
$(document).ready(function () {
    $('input[type="radio"]').click(function () {
        if ($(this).val() == 0) {
            $('input[type="file"]').parent().hide();
            $('input[type="file"]').val('');
            console.log('hide');
        } else {
            $('input[type="file"]').parent().show();
            console.log('show');
        }
    });

    $('#pass1').keyup(function () {

        const regexSpecialChar = /[$&+,:;=?@#|'<>.-^*()%!]/;
        const regexUpperCase = /[A-Z]/;
        const regexNumeric = /[\d]/;

        let container = $('#passwordVerification');
        let pass = $(this).val();
        container.empty();
        let specialChar = regexSpecialChar.test(pass);
        let upperCase = regexUpperCase.test(pass);
        let numeric = regexNumeric.test(pass)

        let message = 'Password must contain ';

        if (!specialChar) {
            message += 'special character, ';
        }
        if (!upperCase) {
            message += 'Uppercase character, ';
        }
        if (!numeric) {
            message += 'numeric character, ';
        }


        if (!specialChar || !upperCase || !numeric) {
            container.append(message)
        }

    });
    // password Match
    $('#pass2').keyup(function () {
        let password = $('#pass1').val();
        let password2 = $(this).val();
        let passwordMatch = $('#passwordMatch');
        let message = 'Password did not match';
        passwordMatch.empty();
        if (password !== password2) {
            $('#submit').prop('disabled', true);
            passwordMatch.append(message);
        } else {
            $('#submit').prop('disabled', false);
        }
    });

});