$("#image").on('change', function () {

    //Get count of selected files
    let countFiles = $(this)[0].files.length;
    let imgPath = $(this)[0].value;
    let extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    let image_holder = $("#imageHolder");


    if (extn === "gif" || extn === "png" || extn === "jpg" || extn === "jpeg") {
        image_holder.empty();
        if (typeof (FileReader) != "undefined") {
            //loop for each file selected for uploaded.
            for (let i = 0; i < countFiles; i++) {
                let reader = new FileReader();
                reader.onload = function (e) {
                    let img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image element
                    let img2 = $('<img/>').addClass('thumb').attr('src', e.target.result)
                    $(image_holder).append(img);
                }
                reader.readAsDataURL($(this)[0].files[i]);
            }

        } else {
            alert("This browser does not support FileReader.");
        }
    } else {
        alert("Pls select only images");
    }
});


'use strict';

;( function ( document, window, index )
{
    var inputs = document.querySelectorAll( '.inputfile' );
    Array.prototype.forEach.call( inputs, function( input )
    {
        var label	 = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener( 'change', function( e )
        {
            var fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                label.querySelector( 'span' ).innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });
        // Firefox bug fix
        input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
        input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
    });
}( document, window, 0 ));


$(document).ready(function(){
    //reset button
    $('#reset').click(function(){
        document.getElementById('addNewItemForm').reset();
        console.log('reset');
    });
});