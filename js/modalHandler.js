$(document).ready(function () {
    //writeCommentButton
    $('#writeCommentButton').click(function () {
        $('#writeComment').addClass('show');
    });

    $('#writeQueryButton').click(function () {
        $('#writeQuery').addClass('show');
    });

    // Close

    $('[class=close]').click(function () {
        $(this).parents().parents().removeClass('show');
    });

    $('[id=cancel]').click(function () {
        $(this).parents().parents().trigger('reset');
        $(this).parents().parents().removeClass('show');
    });
    $('.clickable').on('click', function (event) {
        event.stopPropagation();
        event.stopImmediatePropagation();
        let target = $(this).data('target');
        let id = $(this).data('id');
        if (target == 'modal') {
            $('#' + id).addClass('show');
        }
    });
}); 