<?php
include '../init.php';
$submitSignUp = Util::getParam('submit_signup');
Util::debug($submitSignUp);
if (!empty($submitSignUp)) {
    $fname = Util::getParam('fname');
    $username = Util::getParam('username');
    $password = Util::getParam('pass');
    $role = Util::getParam('role');

    $sql = "SELECT user_id FROM tbl_users WHERE user_email='$username'";
    $result = mysqli_query($conn, $sql);
    $resultCheck = mysqli_num_rows($result);
    if ($resultCheck < 1) {
        if(isset($_FILES['profileImage'])){
            $path = '../upload/';
            $ImageUpload = new Upload($_FILES['profileImage']);
            $ImageUpload->file_new_name_body = "attach_";
            if ($ImageUpload->uploaded) {
                $ImageUpload->Process($path);
                if ($ImageUpload->processed) {
                    //upload success
                    $fileName = ($ImageUpload->file_dst_name);
                } else {
                    echo 'error : ' . $ImageUpload->log;
                }
            }
        }
        $attach = $fileName ? $fileName : '';
        $data = [
            'user_email' => $username,
            'user_fname' => $fname,
            'user_pass' => $password,
            'role' => $role,
            'attach' => $attach
        ];
        
        DBcon::insert(User::TABLE_NAME, $data);
        $content = emailBody($fname, $username);
        $subject = 'Account Verification';
        Email::send($username, $fname, $content, $subject);
        $_SESSION['message'] = ['title' => 'Success', 'message' => 'Your account was successfully registered'];
        header('location: ../index.php');
    } else {
        $redirect = '<a href="../login/register.php">Try Again</a>';
        die('Error: Email Already Exist   ' . $redirect);
    }
}

function emailBody($fname, $username)
{
    $html = '<center>
                <br>
                <h2>Hi ' . $fname . '!</h2>
                <p style="font-family: Arial;">Thank You For Creating a Stop N Stay Account, You are all ready to go!</p>     <p style="font-family: Arial;">Please Click The Link Below to Complete the Final Step</p>
                <br>
               <input type="hidden" name="email_add" id="email_add" value="' . $username . '"/>
               <a href = "http://localhost/serbiz/login/confirm.php?username=' . $username . '">Confirm Account</a>
            </center>';
    return $html;
}
